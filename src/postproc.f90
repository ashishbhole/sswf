module postproc
contains
subroutine calculate_error()
use variables
implicit none
integer :: i
real(kind=dp) :: fact
real(kind=dp) :: sol(1:6), err(1:6)
real(kind=dp) :: err_l1(1:6), err_l2(1:6), err_linf(1:6)
logical :: exist

err_l1 = 0.0d0
err_l2 = 0.0d0
err_linf = -1.0d12

fact = 1.0d0/(1.0d0 + (beta*time)**2)
do i = 1, nc
 !  if(cob(i) == 0)then
      sol(1) = H_0*fact
      sol(2) = beta * fact * (beta * time * cell_coord(1, i) + cell_coord(2, i))
      sol(3) = beta * fact * (beta * time * cell_coord(2, i) - cell_coord(1, i))
      sol(4) = fact * fact * (lambda + gamma * beta**2 * time**2)
      sol(5) = fact * fact * (lambda - gamma) * beta * time
      sol(6) = fact * fact * (gamma + lambda * beta**2 * time**2)

      err(1:6) = dabs(sol(1:6) - prim(1:6, i))
      err_l1 = err_l1 + err*carea(i)
      err_l2 = err_l2 + err*err*carea(i)   
      err_linf = dmax1(err_linf, err)
 !  endif
enddo

err_l2 = dsqrt(err_l2)

inquire(file="conv.txt", exist=exist)
if (exist) then
  open(101, file="conv.txt", status="old", position="append", action="write")
else
  open(101, file="conv.txt", status="new", action="write")
end if
write(101, *) char_len, err_l1, err_l2, err_linf
close(101)

end subroutine calculate_error

subroutine total_entropy_time_evolution
use variables
implicit none
integer, parameter :: fid = 10
integer :: i
real(kind=dp) :: detP, ent1, ent2, tot_ent1, tot_ent2 
logical :: exists

! entropy = sum_over_all_cells( det(P)/h * area )

inquire(file = 'entropy_time_evolution.txt', exist = exists)
if (.not. exists) then
   open(fid, file = 'entropy_time_evolution.txt')
   write(fid, *) '# time, tol_ent, abs(tot_ent), diff'
else
   open(fid, file = 'entropy_time_evolution.txt', access='append')        
endif

tot_ent1 = 0.0d0 ; tot_ent2 = 0.0d0
do i = 1, nc
   detP = (prim(4,i)*prim(6,i) - prim(5,i)*prim(5,i))
   ent1 = detP/cons(1,i)
   ent2 = dabs(detP)/cons(1,i)
   tot_ent1 = tot_ent1 + ent1 * carea(i)
   tot_ent2 = tot_ent2 + ent2 * carea(i)
enddo

write(fid, '(4e15.8)') time, tot_ent1, tot_ent2, tot_ent1 - tot_ent2
   
close(fid)       

end subroutine total_entropy_time_evolution

subroutine save_solution()
use variables
implicit none
integer, parameter :: fid = 10
integer :: i, tcell_vtk(1:3)
real(kind=dp) :: zero = 0.0, fact, ex(1:6, 1:nc)
character(64) :: filename, filename1

print*, 'Saving solution at time and iteration : ', real(time), it
tcell_vtk = (/1, 5, 9/) ! convert from gmsh to vtk types

write(filename, '(a,i7.7,a)') './output/solution_', it, '.vtk'
open(fid, file = trim(filename))
write(fid,'("# vtk DataFile Version 3.0")')
write(fid,'("sswf")')
write(fid,'("ASCII")')
write(fid,'("DATASET UNSTRUCTURED_GRID")')
write(fid,'("FIELD FieldData 2")')
write(fid,'("TIME 1 1 double")')
write(fid,'(e15.8)') time
write(fid,'("CYCLE 1 1 int")')
write(fid,'(i8)') it

write(fid,'("POINTS",i16," float")') nv
do i = 1, nv
   write(fid,*) real(coord(1:2, i)), zero
enddo

write(fid,'("CELLS",i16,i16)') nc, 4*ntria + 5*nquad
do i = 1, nc
   write(fid, *) nve(tcell(i)), cell(1:nve(tcell(i)), i) - 1
enddo

write(fid,'("CELL_TYPES",i16)') nc
do i = 1, nc
   write(fid,*) tcell_vtk(tcell(i))
enddo

fact = 1.0d0/(1.0d0 + (beta*time)**2)
do i = 1, nc
   ex(1,i) = H_0*fact
   ex(2,i) = beta * fact * (beta * time * cell_coord(1, i) + cell_coord(2, i))
   ex(3,i) = beta * fact * (beta * time * cell_coord(2, i) - cell_coord(1, i))
   ex(4,i) = fact * fact * (lambda + gamma * beta**2 * time**2)
   ex(5,i) = fact * fact * (lambda - gamma) * beta * time
   ex(6,i) = fact * fact * (gamma + lambda * beta**2 * time**2)
enddo


write(fid,'("CELL_DATA",i16)') nc

write(fid,'("SCALARS H float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(1, i))
enddo

write(fid,'("SCALARS U float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(2, i))
enddo

write(fid,'("SCALARS V float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(3, i))
enddo

write(fid,'("SCALARS Pxx float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(4, i))
enddo

write(fid,'("SCALARS Pxy float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(5, i))
enddo

write(fid,'("SCALARS Pyy float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) real(prim(6, i))
enddo

if(trim(init_cond) == 'Exact2D')then
write(fid,'("SCALARS eH float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(1,i) - real(prim(1, i))
enddo

write(fid,'("SCALARS eU float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(2,i) - real(prim(2, i))
enddo

write(fid,'("SCALARS eV float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(3,i) - real(prim(3, i))
enddo

write(fid,'("SCALARS ePxx float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(4,i) - real(prim(4, i))
enddo

write(fid,'("SCALARS ePxy float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(5,i) - real(prim(5, i))
enddo

write(fid,'("SCALARS ePyy float 1")')
write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   write(fid, *) ex(6,i) - real(prim(6, i))
enddo

endif

close(fid)

! copy into restart file
write(filename1, '(a)') './output/solution_restart.vtk'
call system('cp '//filename//' '//filename1)

end subroutine save_solution

subroutine read_restart_file()
use variables
implicit none
integer, parameter :: fid = 10
integer :: i, ndata
real :: tmp(1:4)
character(64) :: filename

write(filename, '(a)') './output/solution_restart.vtk'
open(fid, file = trim(filename), status='old', action='read')
read(fid, *)       !write(fid,'("# vtk DataFile Version 3.0")')
read(fid, *)       !write(fid,'("sswf")')
read(fid, *)       !write(fid,'("ASCII")')
read(fid, *)       !write(fid,'("DATASET UNSTRUCTURED_GRID")')
read(fid, *)       !write(fid,'("FIELD FieldData 2")')
read(fid, *)       !write(fid,'("TIME 1 1 double")')
read(fid, *)  time !write(fid,'(e15.8)') time
read(fid, *)       !write(fid,'("CYCLE 1 1 int")')
read(fid, *)  it   !write(fid,'(i8)') it

read(fid, *)   !write(fid,'("POINTS",i16," float")') nv
do i = 1, nv
   read(fid,*) tmp(1:3)
enddo

read(fid, *) !write(fid,'("CELLS",i16,i16)') nc, 4*ntria + 5*nquad
do i = 1, nc
   read(fid, *) ndata, tmp(1:ndata)
enddo

read(fid, *) !write(fid,'("CELL_TYPES",i16)') nc
do i = 1, nc
   read(fid,*) ndata
enddo

read(fid, *) !write(fid,'("CELL_DATA",i16)') nc

read(fid, *) !write(fid,'("SCALARS H float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(1, i)
enddo

read(fid, *) !write(fid,'("SCALARS U float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(2, i)
enddo

read(fid, *) !write(fid,'("SCALARS V float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(3, i)
enddo

read(fid, *) !write(fid,'("SCALARS Pxx float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(4, i)
enddo

read(fid, *) !write(fid,'("SCALARS Pxy float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(5, i)
enddo

read(fid, *) !write(fid,'("SCALARS Pyy float 1")')
read(fid, *) !write(fid,'("LOOKUP_TABLE default")')
do i = 1, nc
   read(fid, *) prim(6, i)
enddo

close(fid)

end subroutine read_restart_file

end module postproc
