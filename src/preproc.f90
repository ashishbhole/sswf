module preproc

contains

subroutine cell_area()
use variables
implicit none
integer :: i, v1, v2, v3, v4
real(kind=dp) :: x1, x2, x3, x4, y1, y2, y3, y4

! calculate area for each cell
allocate(carea(1:nc))

do i = 1, nc
   if(tcell(i) == 2)then
       v1 = cell(1, i)
       v2 = cell(2, i)
       v3 = cell(3, i)

       x1 = coord(1, v1)
       x2 = coord(1, v2)
       x3 = coord(1, v3)

       y1 = coord(2, v1)
       y2 = coord(2, v2)
       y3 = coord(2, v3)

       carea(i) = 0.5d0*((x1-x2)*(y1+y2) + (x2-x3)*(y2+y3) + (x3-x1)*(y3+y1))
   elseif(tcell(i) == 3)then
       v1 = cell(1, i)
       v2 = cell(2, i)
       v3 = cell(3, i)
       v4 = cell(4, i)

       x1 = coord(1, v1)
       x2 = coord(1, v2)
       x3 = coord(1, v3)
       x4 = coord(1, v4)

       y1 = coord(2, v1)
       y2 = coord(2, v2)
       y3 = coord(2, v3)
       y4 = coord(2, v4)

       carea(i) = 0.5d0*((x1-x3)*(y2-y4) + (x4-x2)*(y1-y3))
   else
       print*, "Wrong cell type occured while calculating cell areas at : ", i
       call abort
   endif
enddo

end subroutine cell_area


subroutine cell_center()
use variables
implicit none
integer :: i, j, v
real(kind=dp) :: x1, y1

! calculate center for each cell
allocate(cell_coord(1:2, 1:nc))
cell_coord = 0.0d0

do i = 1, nc
   x1 = 0.0d0 ; y1 = 0.0d0
   do j = 1, nve(tcell(i))
      v = cell(j, i)
      x1 = x1 + coord(1, v)
      y1 = y1 + coord(2, v)
   enddo
   cell_coord(1, i) = x1/dble(nve(tcell(i)))
   cell_coord(2, i) = y1/dble(nve(tcell(i)))
enddo

end subroutine cell_center

! Find IDs of cells (faces in 2D) surrounding every point in the mesh
! and stores in the connected list csup1 and csup2. This information
! is required to generate list of edges. This subroutine is implemented
! from the book Applied CFD techniques by Lohner.

subroutine cells_surrounding_point()
use variables
implicit none
integer :: mcsup, i, j, k, istor

print*, "Generating cells surrounding points "

! assuming maximum 20 cells surrounding a vertex
mcsup = 20*nv
allocate(csup1(1:mcsup))  ! contains ids of cells surrounding each point
allocate(csup2(1:nv+1))   ! contains location and number of cellss surrounding point

! Element Pass 1: Count the number of cells connected to each point
csup2(1:nv+1) = 0
do i = 1, nc               ! loop over number of cells
   do j = 1, nve(tcell(i)) ! loop over number of vertices in each cell
      k = cell(j, i) + 1   ! update storage counter, storing ahead
      csup2(k) = csup2(k) + 1
   enddo
enddo

do i = 2, nv+1             ! Loop over the points
   csup2(i) = csup2(i) + csup2(i-1)      ! Update storage counter and store
enddo

! Element pass 2: Store the elements in csup1
do i = 1, nc
   do j = 1, nve(tcell(i))
      k = cell(j, i)  ! Update storage counter, storing in csup1
      istor = csup2(k)+1
      csup2(k) = istor
      csup1(istor) = i
   enddo
enddo

do i = nv+1, 2, -1
   csup2(i) = csup2(i-1)
enddo
csup2(1) = 0
 
! Check: The following if condition must not be violeted.
do i = 1, nv
   if((csup2(i)+1) .gt. (csup2(i+1)))then
      print*, "Error in cells surrounding points at : ", i, (csup2(i)+1), (csup2(i+1))
      call abort
   endif
enddo

end subroutine cells_surrounding_point

! It generates list of faces (edges in 2D) present in the mesh
! and stores face(1:2, 1:nif). This subroutine is implemented
! from the book Applied CFD techniques by Lohner.
! List of boundary face is appended at the end of this subroutine
! into face(:, :).

subroutine list_of_faces()
use variables
implicit none
integer :: i, j, k, m, n, t, v
integer :: icsup, cid, ifc, ilocal
integer, allocatable :: lpoin(:), inpoe1(:), connect(:,:,:)
logical :: on_boundary = .true.

print*, "Generating list of faces "

! connect() array stores mapping of nodes in the cell
! those connect to form edges
allocate(connect(1:2, 1:4, 1:3))
connect(:, :, 1) = 0

! for tria
connect(1:2, 1, 2) = (/2, 3/)
connect(1:2, 2, 2) = (/3, 1/)
connect(1:2, 3, 2) = (/1, 2/)

! for quad
connect(1:2, 1, 3) = (/2, 4/)
connect(1:2, 2, 3) = (/1, 3/)
connect(1:2, 3, 3) = (/2, 4/)
connect(1:2, 4, 3) = (/1, 3/)

nif = 4*nc  ! assuming there can be maximum 4*nc faces in the mesh
allocate(lpoin(1:nv), face(1:2, 1:nif), inpoe1(1:nv+1))
lpoin(1:nv) = 0; inpoe1(1) = 0 ;  ilocal = 0 ; ifc = 0

face = -1

do i = 1, nv                          ! Loop over the vertices
   do icsup = csup2(i)+1, csup2(i+1)  ! Loop over the cells surrounding a vertex
      cid = csup1(icsup)              ! cell id
      t = tcell(cid)                  ! Type of cell
      do m = 1, nve(tcell(cid))       ! finds local index of point i
         if(i .eq. cell(m, cid))then
            ilocal = m
         endif
      enddo

     ! number of edges forming at each vertex of any cell
     n = 2

     do k = 1, n
        v = connect(k, ilocal, t)    ! local vertex id to form valid edge
        j = cell(v, cid)           ! global vertex id to form valid edge
        if(j.ne.i .and. lpoin(j).ne.i) then  ! to avoid duplicate edges
           ! Update storage counter, storing ahead, and mark lpoin
           if(i .lt. j)then        ! ensure edge(1,i) < edge(j,i)
              call is_on_boundary(i, j, on_boundary)
              if(on_boundary .eqv. .false.) then
                 ifc = ifc + 1
                 face(1:2, ifc) = (/i, j/)
              endif
           endif
           lpoin(j) = i
        endif
     enddo
  enddo
  inpoe1(i+1) = ifc  ! Update storage counters
enddo
deallocate(lpoin,inpoe1)

nif = ifc ! total number of internal edges in the mesh

end subroutine list_of_faces

subroutine is_on_boundary(a, b, on_boundary)
use variables
implicit none
integer, intent(in) :: a, b
logical, intent(out) :: on_boundary
integer :: i

on_boundary = .false.

do i = 1, non_per_nbf
   if( (a == non_per_bface(1, i) .and. b == non_per_bface(2, i)) .or. &
       (b == non_per_bface(1, i) .and. a == non_per_bface(2, i)) )then
       on_boundary = .true.
       exit
   endif
enddo

if(on_boundary .eqv. .true.) return

do i = 1, per_nbf
   if( (a == per_bface(1, i) .and. b == per_bface(2, i)) .or. &
       (b == per_bface(1, i) .and. a == per_bface(2, i)) )then
       on_boundary = .true.
       exit
   endif
enddo

return

end subroutine is_on_boundary

! In a cell centered FV code, we need ID of cells at the both sides of
! the face (edge in 2D). This subroutine generates this information.
! Since we have cells surrounding each point, intersection of the list
! of cells surrounding two points of the face (edge) gives us IDs of two
! cells forming the face (edge).

subroutine cells_surrounding_internal_face()
use variables
implicit none
integer :: i, j, n1, n2, v1, v2, c, coun
integer, allocatable :: list1(:), list2(:), intersection(:)
logical, allocatable :: tmp(:)
real(kind=dp) :: x1, x2, y1, y2, dx, dy, vec_a(1:2), vec_b(1:2)

print*, "forming cells surrounding internal faces "

allocate(cells_sur_iface(1:2, 1:nif))
allocate(anorm_internal(1:2, 1:nif))

cells_sur_iface = -1
anorm_internal = 0.0d0

do i = 1, nif    ! loop over all internal faces

   v1 = face(1, i) ! global vertex id of 1st point of the face
   n1 = size(csup1(csup2(v1)+1:csup2(v1+1))) ! number of cells surrounding vertex v
   allocate(list1(1:n1))
   list1(1:n1) = csup1(csup2(v1)+1:csup2(v1+1))

   v2 = face(2, i) ! global vertex id of 2nd point of the face
   n2 = size(csup1(csup2(v2)+1:csup2(v2+1))) ! number of cells surrounding vertex v
   allocate(list2(1:n2))
   list2(1:n2) = csup1(csup2(v2)+1:csup2(v2+1))

   ! find intersection of list1 and list2. For an internal face it must contains two
   ! cells forming the internal face i. For a boundary face it must contains one
   ! cell to which the face i belong.
   allocate(tmp(1:max(n1, n2)), intersection(1:max(n1, n2)))
   intersection = -1
   coun = 0
   do j = 1, n1
      tmp(j) = any(list1(j) == list2)
      if(tmp(j) .eqv. .true.) then
         coun = coun + 1
         intersection(coun) = list1(j)
      endif
   enddo

   if((intersection(1) .ne. -1) .and. (intersection(1) .ne. -2))then ! internal faces
      cells_sur_iface(1:2, i) = intersection(1:2)
   else
      print*,"Error in cells surrounding an internal face : ", i
      call abort
   endif

   deallocate(tmp, intersection, list1, list2)

   ! vector normal to internal face (edge)
   dx = coord(1, v2) - coord(1, v1)
   dy = coord(2, v2) - coord(2, v1)
   vec_a = (/-dy, dx/)

   ! vector joining two cell centers
   c = cells_sur_iface(1, i)
   x1 = cell_coord(1, c)
   y1 = cell_coord(2, c)

   c = cells_sur_iface(2, i)
   x2 = cell_coord(1, c)
   y2 = cell_coord(2, c)

   vec_b = (/x2-x1, y2-y1/)

   if(dot_product(vec_a, vec_b) .gt. 0.0d0)then
      anorm_internal(1:2, i) = vec_a(1:2)
   else
      anorm_internal(1:2, i) = -vec_a(1:2)
   endif

enddo

end subroutine cells_surrounding_internal_face

! In a cell centered FV code, we need ID of cells at the both sides of
! the face (edge in 2D). This subroutine generates this information.
! Since we have cells surrounding each point, intersection of the list
! of cells surrounding two points of the face (edge) gives us IDs of two
! cells forming the face (edge).

subroutine cells_surrounding_non_per_bface()
use variables
implicit none
integer :: i, j, n1, n2, v1, v2, c, coun
integer, allocatable :: list1(:), list2(:), intersection(:)
logical, allocatable :: tmp(:)
real(kind=dp) :: x, y, dx, dy, vec_a(1:2), vec_b(1:2)

print*, "forming cells surrounding non-periodic boundary faces "

allocate(cells_sur_non_per_bface(1:2, 1:non_per_nbf))
allocate(anorm_non_per(1:2, 1:non_per_nbf))

cells_sur_non_per_bface = -1
anorm_non_per = 0.0d0

ncg  = nc

do i = 1, non_per_nbf    ! loop over all internal faces

   v1 = non_per_bface(1, i) ! global vertex id of 1st point of the non periodic boundary face
   n1 = size(csup1(csup2(v1)+1:csup2(v1+1))) ! number of cells surrounding vertex v
   allocate(list1(1:n1))
   list1(1:n1) = csup1(csup2(v1)+1:csup2(v1+1))

   v2 = non_per_bface(2, i) ! global vertex id of 2nd point of the non periodic boundary face
   n2 = size(csup1(csup2(v2)+1:csup2(v2+1))) ! number of cells surrounding vertex v
   allocate(list2(1:n2))
   list2(1:n2) = csup1(csup2(v2)+1:csup2(v2+1))

   ! find intersection of list1 and list2. For the nonperiodic boundary face it must contains
   ! one cell to which the bface i belong.
   allocate(tmp(1:max(n1, n2)), intersection(1:max(n1, n2)))
   intersection = -1
   coun = 0
   do j = 1, n1
      tmp(j) = any(list1(j) == list2)
      if(tmp(j) .eqv. .true.) then
         coun = coun + 1
         intersection(coun) = list1(j)
      endif
   enddo

   if(intersection(1) .ne. -1)then ! non periodic faces
      cells_sur_non_per_bface(1, i) = intersection(1)
      ncg = ncg + 1
      cells_sur_non_per_bface(2, i) = ncg
   else
      print*,"Error in cells surrounding a non periodic boundary face : ", i
      call abort
   endif
   deallocate(tmp, intersection, list1, list2)

   ! vector normal to boundary face (edge)
   dx = coord(1, v2) - coord(1, v1)
   dy = coord(2, v2) - coord(2, v1)
   vec_a = (/-dy, dx/)

   ! midpoint of the edge
   x = 0.5d0*(coord(1, v2) + coord(1, v1))
   y = 0.5d0*(coord(2, v2) + coord(2, v1))

   ! vector of a cell center and midpoint of the edge
   c = cells_sur_non_per_bface(1, i)

   vec_b = (/x - cell_coord(1, c), y - cell_coord(2, c)/)

   if(dot_product(vec_a, vec_b) .gt. 0.0d0)then
      anorm_non_per(1:2, i) = vec_a(1:2)
   else
      anorm_non_per(1:2, i) = -vec_a(1:2)
   endif

enddo

end subroutine cells_surrounding_non_per_bface

subroutine calculate_characteristic_length()
use variables
implicit none
integer :: i, v1, v2
real(kind=dp) :: dh, dx, dy

char_len = 1.0d12
do i = 1, nif
   v1 = face(1, i)
   v2 = face(2, i)
   dx = coord(1, v2) - coord(1, v1)
   dy = coord(2, v2) - coord(2, v1)
   dh = dsqrt(dx*dx + dy*dy)
   char_len = dmin1(char_len, dh)
enddo

do i = 1, per_nbf
   v1 = per_bface(1, i)
   v2 = per_bface(2, i)
   dx = coord(1, v2) - coord(1, v1)
   dy = coord(2, v2) - coord(2, v1)
   dh = dsqrt(dx*dx + dy*dy)
   char_len = dmin1(char_len, dh)
enddo

do i = 1, non_per_nbf
   v1 = non_per_bface(1, i)
   v2 = non_per_bface(2, i)
   dx = coord(1, v2) - coord(1, v1)
   dy = coord(2, v2) - coord(2, v1)
   dh = dsqrt(dx*dx + dy*dy)
   char_len = dmin1(char_len, dh)
enddo

end subroutine calculate_characteristic_length

subroutine periodic_faces()
use variables
implicit none
integer :: i, j, v1, v2, v3, v4
real(kind=dp) :: m1 = 0.0d0, m2 = 0.0d0, eps = 1.0d-10

print*, "!------------------------------------------"
print*, "forming periodic faces "

allocate(pface_list(1:per_nbf))
pface_list = -1

! for x-direction
if(trim(per_dir) == 'x')then
   do i = 1, per_nbf    ! loop over all boundary faces

      ! if bface is on slave boundaries
      if(per_btag(i) == x_per_tag1)then

         v1 = per_bface(1, i)
         v2 = per_bface(2, i)
         do j = 1, per_nbf ! loop over all boundary faces

            ! if bface is on master boundaries
            if(per_btag(j) == x_per_tag2)then
                v3 = per_bface(1, j)
                v4 = per_bface(2, j)

                ! find x coordinate of mid-points of two bfaces
                m1 = 0.5d0*(coord(1, v1) + coord(1, v2))
                m2 = 0.5d0*(coord(1, v3) + coord(1, v4))

                ! if found periodic, form a pair of periodic faces
                if(dabs(m1-m2) .lt. eps)then
                   pface_list(i) = j
                endif

            endif  ! bface on masters
         enddo  ! end loop over j
      endif  ! bface on slaves
   enddo  ! end loop over i
endif


! for y-direction
if(trim(per_dir) == 'y')then
   do i = 1, per_nbf    ! loop over all boundary faces

      ! if bface is on slave boundaries
      if(per_btag(i) == y_per_tag1)then

         v1 = per_bface(1, i)
         v2 = per_bface(2, i)
         do j = 1, per_nbf ! loop over all boundary faces

            ! if bface is on master boundaries
            if(per_btag(j) == y_per_tag2)then
                v3 = per_bface(1, j)
                v4 = per_bface(2, j)

                ! find y coordinate of mid-points of two bfaces
                m1 = 0.5d0*(coord(2, v1) + coord(2, v2))
                m2 = 0.5d0*(coord(2, v3) + coord(2, v4))

                ! if found periodic, form a pair of periodic faces
                if(dabs(m1-m2) .lt. eps)then
                   pface_list(i) = j
                endif

            endif  ! bface on masters
         enddo  ! end loop over j
      endif  ! bface on slaves
   enddo  ! end loop over i
endif

! for both xy-direction
if(trim(per_dir) == 'xy')then
   do i = 1, per_nbf    ! loop over all boundary faces

      ! if bface is on slave boundaries
      if(per_btag(i) == x_per_tag1 .or. per_btag(i) == y_per_tag1)then

         v1 = per_bface(1, i)
         v2 = per_bface(2, i)
         do j = 1, per_nbf ! loop over all boundary faces

            if(per_btag(j) == x_per_tag2)then

               v3 = per_bface(1, j)
               v4 = per_bface(2, j)
               ! find x coordinate of mid-points of two bfaces
               m1 = 0.5d0*(coord(2, v1) + coord(2, v2))
               m2 = 0.5d0*(coord(2, v3) + coord(2, v4))

               ! if found periodic, form a pair of periodic faces
               if(dabs(m1-m2) .lt. eps)then
                  pface_list(i) = j
               endif
            elseif(per_btag(j) == y_per_tag2)then
               v3 = per_bface(1, j)
               v4 = per_bface(2, j)

               ! find y coordinate of mid-points of two bfaces
               m1 = 0.5d0*(coord(1, v1) + coord(1, v2))
               m2 = 0.5d0*(coord(1, v3) + coord(1, v4))

               ! if found periodic, form a pair of periodic faces
               if(dabs(m1-m2) .lt. eps)then
                   pface_list(i) = j
               endif
            endif  ! bface on masters

         enddo  ! end loop over j
      endif  ! bface on slaves
   enddo  ! end loop over i
endif
end subroutine periodic_faces

subroutine cells_surrounding_per_bface()
use variables
implicit none
integer :: i, j, n1, n2, v1, v2, c, coun
integer, allocatable :: list1(:), list2(:), intersection(:)
logical, allocatable :: tmp(:)
real(kind=dp) :: x1, x2, y1, y2, dx, dy, vec_a(1:2), vec_b(1:2)

print*, "forming cells surrounding periodic boundary faces "

allocate(cells_sur_per_bface(1:2, per_nbf))
allocate(anorm_per(1:2, 1:per_nbf))

cells_sur_per_bface = -1
anorm_per = 0.0d0

do i = 1, per_nbf    ! loop over all internal faces

   if(pface_list(i) .ne. -1)then
      v1 = per_bface(1, i) ! global vertex id of 1st point of the periodic boundary face
      n1 = size(csup1(csup2(v1)+1:csup2(v1+1))) ! number of cells surrounding vertex v
      allocate(list1(1:n1))
      list1(1:n1) = csup1(csup2(v1)+1:csup2(v1+1))

      v2 = per_bface(2, i) ! global vertex id of 2nd point of the periodic boundary face
      n2 = size(csup1(csup2(v2)+1:csup2(v2+1))) ! number of cells surrounding vertex v
      allocate(list2(1:n2))
      list2(1:n2) = csup1(csup2(v2)+1:csup2(v2+1))

      ! find intersection of list1 and list2. For the nonperiodic boundary face it must contains
      ! one cell to which the bface i belong. 
      allocate(tmp(1:max(n1, n2)), intersection(1:max(n1, n2)))
      intersection = -1
      coun = 0
      do j = 1, n1
         tmp(j) = any(list1(j) == list2)
         if(tmp(j) .eqv. .true.) then
            coun = coun + 1
            intersection(coun) = list1(j)
         endif
      enddo

      if(intersection(1) .ne. -1)then ! periodic faces
         cells_sur_per_bface(1, i) = intersection(1)
      else
         print*,"Error in cells surrounding a periodic boundary face : ", i
         call abort
      endif

      deallocate(tmp, intersection, list1, list2)


      v1 = per_bface(1, pface_list(i)) ! global vertex id of 1st point of the periodic boundary face
      n1 = size(csup1(csup2(v1)+1:csup2(v1+1))) ! number of cells surrounding vertex v
      allocate(list1(1:n1))
      list1(1:n1) = csup1(csup2(v1)+1:csup2(v1+1))

      v2 = per_bface(2, pface_list(i)) ! global vertex id of 2nd point of the periodic boundary face
      n2 = size(csup1(csup2(v2)+1:csup2(v2+1))) ! number of cells surrounding vertex v
      allocate(list2(1:n2))
      list2(1:n2) = csup1(csup2(v2)+1:csup2(v2+1))

      ! find intersection of list1 and list2. For the nonperiodic boundary face it must contains
      ! one cell to which the bface i belong. 
      allocate(tmp(1:max(n1, n2)), intersection(1:max(n1, n2)))
      intersection = -1
      coun = 0
      do j = 1, n1
         tmp(j) = any(list1(j) == list2)
         if(tmp(j) .eqv. .true.) then
            coun = coun + 1
            intersection = list1(j)
         endif
      enddo

      if(intersection(1) .ne. -1)then ! internal faces
         cells_sur_per_bface(2, i) = intersection(1)
      else
         print*,"Error in cells surrounding a periodic boundary face : ", i
         call abort
      endif

      deallocate(tmp, intersection, list1, list2)

      ! vector normal to boundary face (edge)
      v1 = per_bface(1, i) 
      v2 = per_bface(2, i)       
      dx = coord(1, v2) - coord(1, v1)
      dy = coord(2, v2) - coord(2, v1)
      vec_a = (/-dy, dx/)

      ! midpoint of the edge
      x2 = 0.5d0*(coord(1, v1) + coord(1, v2))
      y2 = 0.5d0*(coord(2, v1) + coord(2, v2))

      ! vector a cell center and midpoint of the edge
      c = cells_sur_per_bface(1, i)
      x1 = cell_coord(1, c)
      y1 = cell_coord(2, c)

      vec_b = (/x2-x1, y2-y1/)

      if(dot_product(vec_a, vec_b) .gt. 0.0d0)then
         anorm_per(1:2, i) = vec_a(1:2)
      else
         anorm_per(1:2, i) = -vec_a(1:2)
      endif
      
   endif
enddo

end subroutine cells_surrounding_per_bface

subroutine cells_on_boundaries()
use variables
implicit none
integer :: ic, v1, v2
logical :: on_boundary

print*, "determining internal and boundary cells"      

allocate(cob(1:nc))
cob = 0

do ic = 1, nc
   if(tcell(ic) == 2)then

      v1 = cell(1, ic)
      v2 = cell(2, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then
         cob(ic) = cob(ic) + 1
      endif

      v1 = cell(2, ic)
      v2 = cell(3, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif

      v1 = cell(3, ic)
      v2 = cell(1, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif
      
   elseif(tcell(ic) == 3)then

      v1 = cell(1, ic)
      v2 = cell(2, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif

      v1 = cell(2, ic)
      v2 = cell(3, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif

      v1 = cell(3, ic)
      v2 = cell(4, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif

      v1 = cell(4, ic)
      v2 = cell(1, ic)
      call is_on_boundary(v1, v2, on_boundary)
      if(on_boundary .eqv. .true.)then 
         cob(ic) = cob(ic) + 1
      endif

   else

      print*, "error in the cells_on_boundaries"
      call abort

   endif
enddo

end subroutine cells_on_boundaries

subroutine check_if_cells_outward_normals_close()
use variables
implicit none
integer :: i, c, c1, c2
real(kind=dp) :: eps = 1.0d-10
real(kind=dp), allocatable :: normal(:,:)

print*, "checking if cell outwords vanish for entire grid "

allocate(normal(1:2, 1:nc))

normal = 0.0d0

do i = 1, nif
   c1 = cells_sur_iface(1, i)
   c2 = cells_sur_iface(2, i)
   normal(:, c1) = normal(:, c1) + anorm_internal(:, i)
   normal(:, c2) = normal(:, c2) - anorm_internal(:, i)
enddo

do i = 1, non_per_nbf
   c = cells_sur_non_per_bface(1, i)
   normal(:, c) = normal(:, c) + anorm_non_per(:, i)
enddo

if(trim(bound_cond) == 'Roll2D')then
   do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)
      if(pface_list(i) .ne. -1)then
         if(per_btag(i) == y_per_tag1)then
           c1 = cells_sur_per_bface(1, i)
           c2 = cells_sur_per_bface(2, i)
           normal(:, c1) = normal(:, c1) + anorm_per(:, i)
           normal(:, c2) = normal(:, c2) - anorm_per(:, i)
         elseif(per_btag(i) == y_per_tag2)then
         else
         endif
      endif
   enddo
elseif(trim(bound_cond) == 'Roll1D')then
   do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)
      if(pface_list(i) .ne. -1)then
         if(per_btag(i) == x_per_tag1 .or. per_btag(i) == y_per_tag1)then
           c1 = cells_sur_per_bface(1, i)
           c2 = cells_sur_per_bface(2, i)
           normal(:, c1) = normal(:, c1) + anorm_per(:, i)
           normal(:, c2) = normal(:, c2) - anorm_per(:, i)
         else
         endif
      endif
   enddo
elseif(trim(bound_cond) == 'Damb1D')then
   do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)
      if(pface_list(i) .ne. -1)then
         if(per_btag(i) == x_per_tag1)then
           c1 = cells_sur_per_bface(1, i)
           c2 = cells_sur_per_bface(2, i)
           normal(:, c1) = normal(:, c1) + anorm_per(:, i)
           normal(:, c2) = normal(:, c2) - anorm_per(:, i)
         else
         endif
      endif
   enddo
endif

print*, maxval(dabs(normal(1, :))), maxval(dabs(normal(2, :)))

if( (maxval(dabs(normal(1, :))) .gt. eps) .or. &
    (maxval(dabs(normal(2, :))) .gt. eps) ) then
    print*, "Something is wrong with cell normals"
    call abort
endif

deallocate(normal)
end subroutine check_if_cells_outward_normals_close

subroutine cells_surrounding_cell()
use variables
implicit none
integer :: mcsuc, i, j, c, c1, c2, f, v1, v2, icount

print*, "Generating cells surrounding a cell "

! face topology in triangle
face_in_cell = -1
face_in_cell(1:2, 1, 2) = (/1, 2/)
face_in_cell(1:2, 2, 2) = (/2, 3/)
face_in_cell(1:2, 3, 2) = (/3, 1/)

! face topology in quad
face_in_cell(1:2, 1, 3) = (/1, 2/)
face_in_cell(1:2, 2, 3) = (/2, 3/)
face_in_cell(1:2, 3, 3) = (/3, 4/)
face_in_cell(1:2, 4, 3) = (/4, 1/)

! assuming maximum 4 cells surrounding a cell
mcsuc = 4*nc
allocate(csuc1(1:mcsuc))  ! contains ids of cells surrounding each point
allocate(csuc2(1:nc+1))   ! contains location and number of cells surrounding cell

csuc1 = 0; csuc2 = 0; icount = 0

! Loop over cells
do c = 1, nc
   ! Loop over faces in the cell
   do f = 1, nve(tcell(c))
      ! two vertices of the face
      v1 = face_in_cell(1, f, tcell(c))
      v1 = cell(v1, c)
      v2 = face_in_cell(2, f, tcell(c))
      v2 = cell(v2, c)
      ! loop over first vertex
      do i = csup2(v1)+1, csup2(v1+1)
         c1= csup1(i)
         !loop over second vertex
         do j = csup2(v2)+1, csup2(v2+1)
            c2= csup1(j)
            ! if c1 is in csup(v2) and not equal to c
            if((c1 .ne. c) .and. (c1 == c2))then
               icount = icount+1
               csuc1(icount) = c1
            endif
         enddo
      enddo
   enddo
   csuc2(c+1) = icount
enddo

!do i = 1, nc
!   print*, nc, i, size(csuc1(csuc2(i)+1:csuc2(i+1))), csuc1(csuc2(i)+1:csuc2(i+1))
!enddo

end subroutine cells_surrounding_cell

! adapted from point surrounding point algorithm
! in Applied CFD techniques by Lohner.

subroutine extended_cells_surrounding_cell()
use variables
implicit none
integer :: mcsuc, i, j, v, k, c, icount
integer, allocatable :: flag(:)

print*, "Generating extended cells surrounding a cell "

! assuming maximum 20 cells surrounding a cell
mcsuc = 20*nc
allocate(csuc1(1:mcsuc))  ! contains ids of cells surrounding each point
allocate(csuc2(1:nc+1))   ! contains location and number of cells surrounding cell
allocate(flag(1:nc))

csuc1 = 0; csuc2 = 0; flag = 0; icount = 0

! loop over cells
do i = 1, nc
   ! loop over points in the cell
   do j = 1, nve(tcell(i))
      v = cell(j ,i)
      ! loop over cells surrounding the point
      do k = csup2(v)+1, csup2(v+1)
         c = csup1(k)
         ! ignore if it is the same cell or already flagged
         if((c .ne. i) .and. (flag(c) .ne. i))then
            icount = icount + 1
            csuc1(icount) = c
            flag(c) = i
         endif
      enddo
   enddo
   csuc2(i+1) = icount
enddo

!do i = 1, nc
!   print*, nc, i, size(csuc1(csuc2(i)+1:csuc2(i+1))), csuc1(csuc2(i)+1:csuc2(i+1))
!enddo

deallocate(flag)

end subroutine extended_cells_surrounding_cell

subroutine read_gmsh_grid()
use variables
implicit none
real :: ztmp
integer :: i, j, list(1:4)
character(len=256) :: string

print*, "!------------------------------------------"
print*, "Reading gmsh grid file ", trim(gridfile)

open(10, file=trim(gridfile), status='old')
! skip some lines
string = " "
do while (trim(string) /= "$Nodes")
   read(10,  *) string
enddo

read(10, *) nv        ! read no of vertices
allocate(coord(1:2, 1:nv))

do i = 1, nv
   read(10,*) j, coord(1:2,i), ztmp  ! read coordinates of each vertex
enddo

! skip some lines
string = " "
do while (trim(string) /= "$Elements")
   read(10, *) string
enddo

read(10, *) nelem      ! read no of elements boundary faces + cells

allocate(telem(1:nelem), tface(1:nelem), tcell(1:nelem))
allocate(per_bface(1:2, 1:nelem), non_per_bface(1:2, 1:nelem), cell(1:4, 1:nelem))
allocate(per_btag(1:nelem), non_per_btag(1:nelem), tag_list(1:3))
allocate(bvert_flag(1:nv))

bvert_flag = -1
nc = 0; tcell = 0 ; ntria = 0; nquad = 0
per_nbf = 0; non_per_nbf = 0

do i = 1, nelem

   read(10,*) j, telem(i), ntags, (tag_list(j), j=1,ntags), (list(j), j=1,nve(telem(i)))

   if(telem(i) == 15)then ! we ignore elm-type 15 which is 1-node point
   elseif(telem(i) == 1)then ! read boundary edges
      if( ((tag_list(1) == x_per_tag1) .or. (tag_list(1) == x_per_tag2)) .or. &
        & ((tag_list(1) == y_per_tag1) .or. (tag_list(1) == y_per_tag2)) ) then
         per_nbf = per_nbf + 1
         per_btag(per_nbf) = tag_list(1)  ! stores boudary face tag
         per_bface(1:2, per_nbf) = list(1:2)
      else
         non_per_nbf = non_per_nbf + 1
         non_per_btag(non_per_nbf) = tag_list(1)  ! stores boudary face tag
         non_per_bface(1:2, non_per_nbf) = list(1:2)
      endif
      bvert_flag(list(1)) = 1
      bvert_flag(list(2)) = 1
   elseif(telem(i) == 2 .or. telem(i) == 3) then ! read tri and quad cells
      nc = nc + 1             ! cell count
      tcell(nc) = telem(i)    ! type of cell
      if(telem(i) == 2) ntria = ntria + 1     ! measure no of trias
      if(telem(i) == 3) nquad = nquad + 1     ! measure no of quads
      cell(1:nve(tcell(nc)), nc) = list(1:nve(tcell(nc)))
   else
      print*, '("invalid element type in msh file")'
      stop
   endif
enddo

close(10)

print*, " No of vertices             : ", nv
print*, " No of triangles            : ", ntria
print*, " No of quads                : ", nquad
print*, " Total no of boundary edges : ", non_per_nbf + per_nbf
print*, " Total no of cells          : ", nc
print*, "!------------------------------------------"
print*, "Finished Reading periodic information from ", trim(gridfile)

end subroutine read_gmsh_grid


end module preproc
