module auxillary_conditions
contains

subroutine initial_condition()
use variables
implicit none
real(kind=dp) :: U_0
integer :: i
real(kind=dp) :: m, k, eps = 1.d-12

print*, "!------------------------------------------"
print*, "setting initial conditions: ", trim(init_cond)

select case (trim(init_cond))
case('Roll2D')
  m = 1.0d0 ; k = 1.0d0
  U_0 = dsqrt(g * dtan(angle) * H_0 / cf)
  do i = 1, nc
     prim(1, i) = H_0 * (1.0d0 + amp*( dsin(2.0d0*pi*m*cell_coord(1, i)/Lx) + &
                                     & dsin(2.0d0*pi*k*cell_coord(2, i)/Ly) ) )
     prim(2, i) = U_0
     prim(3, i) = 0.0d0
     prim(4, i) = 0.5d0 * phi * prim(1, i) * prim(1, i)
     prim(5, i) = 0.0d0
     prim(6, i) = 0.5d0 * phi * prim(1, i) * prim(1, i)
  enddo

case('Roll1D')
  m = 1.0d0 
  U_0 = dsqrt(g * dtan(angle) * H_0 / cf)
  do i = 1, nc
     prim(1, i) = H_0 * (1.0d0 + amp*( dsin(2.0d0*pi*m*cell_coord(1, i)/Lx) ) )
     prim(2, i) = U_0
     prim(3, i) = 0.0d0
     prim(4, i) = 0.0d0 * phi * prim(1, i) * prim(1, i)
     prim(5, i) = 0.0d0
     prim(6, i) = 0.0d0 
  enddo

case('DamB1D')
  do i = 1, nc
     if( (cell_coord(1, i)-0.5d0) .lt. eps)then
         prim(1, i) = 0.02d0
     else
         prim(1,i) = 0.01d0
     endif
     prim(2, i) = 0.0d0
     prim(3, i) = 0.0d0
     prim(4, i) = 1.0d-4
     prim(5, i) = 0.0d0
     prim(6, i) = 1.0d-4
  enddo

case('Shear1D')
  do i = 1, nc
     prim(1, i) = 0.01d0
     prim(2, i) = 0.0d0
     if( (cell_coord(1, i)-0.5d0) .lt. eps)then
         prim(3, i) = 0.2d0
     else
         prim(3,i) = -0.2d0
     endif
     prim(4, i) = 1.0d-4
     prim(5, i) = 0.0d0
     prim(6, i) = 1.0d-4
  enddo
  
case('Exact2D')
  do i = 1, nc
     prim(1, i) = H_0
     prim(2, i) = beta*cell_coord(2, i)
     prim(3, i) = -beta*cell_coord(1, i)
     prim(4, i) = lambda
     prim(5, i) = 0.0d0
     prim(6, i) = gamma 
  enddo
  
end select

do i = 1, nc
   prim(7,  i) = 0.5d0 * ( g*prim(1, i) + prim(4, i) + Prim(6, i) )
   prim(8,  i) = dsqrt( g *prim(1, i) + 3.0d0 * prim(4, i) )
   prim(9,  i) = dsqrt( prim(4, i) )
   prim(10, i) = 0.5d0 * g * prim(1, i)**2  + prim(4, i) * prim(1, i)
enddo

print*, "!------------------------------------------"
end subroutine initial_condition

subroutine boundary_condition(tb)
use variables
use solver
implicit none
real(kind=dp), intent(in) :: tb
integer :: i, c1, c2, v1, v2
real(kind=dp), dimension(1:6) :: primL, primR, fluxL, fluxR
real(kind=dp) :: fact, anorm(1:2), xm, ym

fact = 0.0d0

c1 = -1 ; c2 = -1

select case (trim(bound_cond))
case('Roll2D')

do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)

   if(pface_list(i) .ne. -1)then
      if(per_btag(i) == y_per_tag1)then
         c1 = cells_sur_per_bface(1, i)
         c2 = cells_sur_per_bface(2, i)
         ! left and right states
         primL(1:6) = prim(1:6, c1)
         primR(1:6) = prim(1:6, c2)

         fluxL = 0.0d0 ; fluxR = 0.0d0
         anorm(1:2) = anorm_per(1:2,i)
         call riemann_solvers(anorm_per(1:2,i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))
         res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2))
         res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2))
      elseif(per_btag(i) == y_per_tag2)then
      else
      endif
   endif

enddo

do i = 1, non_per_nbf ! loop over all non periodic boundary faces (edges in 2D)

   c1 = cells_sur_non_per_bface(1, i)
   c2 = cells_sur_non_per_bface(2, i)

   ! Neumann condition
   prim(:, c2) = prim(:, c1)

   ! rigid condition
   prim(3, c2) = -prim(3, c1)    ! v
   prim(5, c2) = -prim(5, c1)    ! pxy

   ! left and right states
   primL(1:6) = prim(1:6, c1)
   primR(1:6) = prim(1:6, c2)

   fluxL = 0.0d0 ; fluxR = 0.0d0
   anorm = anorm_non_per(1:2, i)
   call riemann_solvers(anorm_non_per(1:2, i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))
   res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2))
   res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2))

enddo

case('Roll1D')
do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)

   if(pface_list(i) .ne. -1)then
      if(per_btag(i) == x_per_tag1 .or. per_btag(i) == y_per_tag1)then
         c1 = cells_sur_per_bface(1, i)
         c2 = cells_sur_per_bface(2, i)

         if(trim(recon) == 'second')then
            call reconstruct_per_boundaries(i, primL(1:6), primR(1:6))
         else
            ! left and right states
            primL(1:6) = prim(1:6, c1)
            primR(1:6) = prim(1:6, c2)
         endif

         fluxL = 0.0d0 ; fluxR = 0.0d0
         call riemann_solvers(anorm_per(1:2,i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))

         res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm_per(1,i)**2 + anorm_per(2,i)**2)
         res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm_per(1,i)**2 + anorm_per(2,i)**2)
      else
      endif
   endif

enddo


case('DamB1D')

do i = 1, non_per_nbf ! loop over all non periodic boundary faces (edges in 2D)

   c1 = cells_sur_non_per_bface(1, i)
   c2 = cells_sur_non_per_bface(2, i)

   ! Neumann condition
   prim(:, c2) = prim(:, c1)

   ! left and right states
   primL(1:6) = prim(1:6, c1)
   primR(1:6) = prim(1:6, c2)

   fluxL = 0.0d0 ; fluxR = 0.0d0
   call riemann_solvers(anorm_non_per(1:2, i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))
   res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm_non_per(1,i)**2 + anorm_non_per(2,i)**2)
   res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm_non_per(1,i)**2 + anorm_non_per(2,i)**2)

enddo

do i = 1, per_nbf ! loop over all periodic boundary faces (edges in 2D)

   if(pface_list(i) .ne. -1)then

      if(per_btag(i) == x_per_tag1)then

        c1 = cells_sur_per_bface(1, i)
        c2 = cells_sur_per_bface(2, i)

         if(trim(recon) == 'second')then
            call reconstruct_per_boundaries(i, primL(1:6), primR(1:6))
         else
            ! left and right states
            primL(1:6) = prim(1:6, c1)
            primR(1:6) = prim(1:6, c2)
         endif

        fluxL = 0.0d0 ; fluxR = 0.0d0
        call riemann_solvers(anorm_per(1:2, i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))
        res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm_per(1,i)**2 + anorm_per(2,i)**2)
        res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm_per(1,i)**2 + anorm_per(2,i)**2)

      elseif(per_btag(i) == y_per_tag2)then
      else
      endif

   endif

enddo

case('Exact2D')

do i = 1, non_per_nbf ! loop over all non periodic boundary faces (edges in 2D)

   c1 = cells_sur_non_per_bface(1, i)
   primL(1:6) = prim(1:6, c1)

   if(trim(recon) == 'second') call reconstruct_on_boundaries(i, primL(1:6))
   
   v1 = non_per_bface(1, i)
   v2 = non_per_bface(2, i)
   xm = 0.5d0*(coord(1, v1) + coord(1, v2))
   ym = 0.5d0*(coord(2, v1) + coord(2, v2))

   ! left and right states
   fact = 1.0d0/(1.0d0 + (beta*tb)**2)
   primR(1) = H_0*fact
   primR(2) = beta * fact * (beta * tb * xm + ym)
   primR(3) = beta * fact * (beta * tb * ym - xm)
   primR(4) = fact * fact * (lambda + gamma * beta**2 * tb**2)
   primR(5) = fact * fact * (lambda - gamma) * beta * tb
   primR(6) = fact * fact * (gamma + lambda * beta**2 * tb**2)
   res(1:6, c1) = 0.0d0

   fluxL = 0.0d0 ; fluxR = 0.0d0
   call riemann_solvers(anorm_non_per(1:2, i), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6))
   res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm_non_per(1,i)**2 + anorm_non_per(2,i)**2)
         
enddo

end select
end subroutine boundary_condition

end module auxillary_conditions
