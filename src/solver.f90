module solver
contains

subroutine calculate_residue()
use variables
implicit none
integer :: ie, c1, c2
real(kind=dp) :: primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6), anorm(1:2)

! loop over all internal faces (edges in 2D)
do ie = 1, nif

   c1 = cells_sur_iface(1, ie)
   c2 = cells_sur_iface(2, ie) 
   anorm = anorm_internal(1:2, ie)
   primL(1:6) = prim(1:6, c1)
   primR(1:6) = prim(1:6, c2)
   fluxL = 0.0d0
   fluxR = 0.0d0 
   if(trim(recon) == 'second')then
      call reconstruct(ie, primL(1:6), primR(1:6))
      call riemann_solvers(anorm, primL, primR, fluxL, fluxR)
   else
      call riemann_solvers(anorm, primL, primR, fluxL, fluxR)      
   endif
   res(1:6, c1) = res(1:6, c1) + fluxL(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2))
   res(1:6, c2) = res(1:6, c2) - fluxR(1:6) * dsqrt(anorm(1)*anorm(1) + anorm(2)*anorm(2)) 
enddo

end subroutine calculate_residue   

subroutine rie_sol(V, primL, primR, fluxL, fluxR)
use variables
implicit none
real(kind=dp), intent(in) :: V(1:2), primL(1:6), primR(1:6)
real(kind=dp), intent(out) :: fluxL(1:6), fluxR(1:6)

real(kind=dp) :: Vno(1:2), Vmo(1:2)
real(kind=dp) :: hl, uxl, uyl, pxxl, pxyl, pyyl, cl, ml, sl, pl
real(kind=dp) :: hr, uxr, uyr, pxxr, pxyr, pyyr, cr, mr, sr, pr
real(kind=dp) :: ul, vl, ur, vr, Pnnl, Pnml, Pmml, Pnnr, Pnmr, Pmmr
real(kind=dp) :: hlS, uxlS, uylS, pxxlS, pxylS, pyylS
real(kind=dp) :: hrS, uxrS, uyrS, pxxrS, pxyrS, pyyrS
real(kind=dp) :: ulS, vlS, pnnlS, pnmlS, pmmlS
real(kind=dp) :: urS, vrS, pnnrS, pnmrS, pmmrS
real(kind=dp) :: pS, uS
real(kind=dp) :: l1, l2, l3, r1, r2, r3
real(kind=dp) :: Vpl(1:6), VplS(1:6), VprS(1:6), Vpr(1:6)

! unit normal to the edge
Vno = V/dsqrt(V(1)**2 + V(2)**2) 
!Vmo(1) = Vno(2) ; Vmo(2) = - Vno(1)
Vmo(1) = -Vno(2) ; Vmo(2) = Vno(1)

! left and right states
hl   = primL(1)     ;  hr   = primR(1)
uxl  = primL(2)     ;  uxr  = primR(2)
uyl  = primL(3)     ;  uyr  = primR(3)

! coordinate transformation of u, v, P
ul = uxl*Vno(1) + uyl*Vno(2)
vl = uxl*Vmo(1) + uyl*Vmo(2)

ur = uxr*Vno(1) + uyr*Vno(2)
vr = uxr*Vmo(1) + uyr*Vmo(2)

pxxl = 0.0d0 ; pxyl = 0.0d0 ; pyyl = 0.0d0
pxxr = 0.0d0 ; pxyr = 0.0d0 ; pyyr = 0.0d0
pnnl = 0.0d0 ; pnml = 0.0d0 ; pmml = 0.0d0
pnnr = 0.0d0 ; pnmr = 0.0d0 ; pmmr = 0.0d0

! acoustic subsystem
pl = hl * (0.5d0*g*hl) ; pr = hr * (0.5d0*g*hr)
cl = dsqrt(g*hl) ; cr = dsqrt(g*hr)
sr = dmax1(ul+cl, ur+cr) ; sl = dmin1(ul-cl, ur-cr)
ml = hl*(ul-sl) ; mr = hr*(ur-sr)
uS  = ( ul*ml - ur*mr +    pl-pr    )/(ml-mr)
pS  = ( (ul-ur)*mr*ml + mr*pl-ml*pr )/(mr-ml)

hlS   = ml/(uS-sl) ; hrS   = mr/(uS-sr)
uls   = us         ; urs   = us
vls   = vl         ; vrs   = vr

pnnlS = 0.0d0 ; pnmlS = 0.0d0 ; pmmlS = 0.0d0
pnnrS = 0.0d0 ; pnmrS = 0.0d0 ; pmmrS = 0.0d0
pxxlS = 0.0d0 ; pxylS = 0.0d0 ; pyylS = 0.0d0
pxxrS = 0.0d0 ; pxyrS = 0.0d0 ; pyyrS = 0.0d0

uxls  = uls*Vno(1) + vls*Vmo(1)
uylS  = uls*Vno(2) + vls*Vmo(2)

uxrs  = urs*Vno(1) + vrs*Vmo(1)
uyrS  = urs*Vno(2) + vrs*Vmo(2)

Vpl  = (/hl ,  hl*uxl ,  hl*uyl , pxxl , pxyl ,  pyyl /)
VplS = (/hlS, hlS*uxlS, hlS*uylS, pxxlS, pxylS,  pyylS/)
VprS = (/hrS, hrS*uxrS, hrS*uyrS, pxxrS, pxyrS,  pyyrS/)
Vpr  = (/hr ,  hr*uxr ,  hr*uyr , pxxr , pxyr ,  pyyr /)

l1 = dmin1(Sl, 0.0d0); l2 = dmin1(uS, 0.0d0); l3 = dmin1(Sr, 0.0d0)
r1 = dmax1(Sl, 0.0d0); r2 = dmax1(uS, 0.0d0); r3 = dmax1(Sr, 0.0d0)

fluxL = -l1*(Vpl - VplS) - l2*(VplS - VprS) - l3*(VprS - Vpr)
fluxR =  r1*(Vpl - VplS) + r2*(VplS - VprS) + r3*(VprS - Vpr)

return
end subroutine rie_sol     

! Originally witten by Prof. Boniface Nkonga
subroutine riemann_solvers(V, primL, primR, fluxL, fluxR)
use variables
implicit none
real(kind=dp), intent(in) :: V(1:2), primL(1:6), primR(1:6)
real(kind=dp), intent(out) :: fluxL(1:6), fluxR(1:6)

real(kind=dp) :: Vno(1:2), Vmo(1:2)
real(kind=dp) :: hl, uxl, uyl, pxxl, pxyl, pyyl, cl, ml, sl, pl
real(kind=dp) :: hr, uxr, uyr, pxxr, pxyr, pyyr, cr, mr, sr, pr
real(kind=dp) :: ul, vl, ur, vr, Pnnl, Pnml, Pmml, Pnnr, Pnmr, Pmmr
real(kind=dp) :: hlS, uxlS, uylS, pxxlS, pxylS, pyylS
real(kind=dp) :: hrS, uxrS, uyrS, pxxrS, pxyrS, pyyrS
real(kind=dp) :: ulS, vlS, pnnlS, pnmlS, pmmlS
real(kind=dp) :: urS, vrS, pnnrS, pnmrS, pmmrS
real(kind=dp) :: pS, uS, ElS, ErS, El, Er, vS, hpnmS
real(kind=dp) :: l1, l2, l3, r1, r2, r3
real(kind=dp) :: Vpl(1:6), VplS(1:6), VprS(1:6), Vpr(1:6)
real(kind=dp) :: fact = 1.0d0

! unit normal to the edge
Vno = V/dsqrt(V(1)**2 + V(2)**2) 
!Vmo(1) = Vno(2) ; Vmo(2) = - Vno(1)
Vmo(1) = -Vno(2) ; Vmo(2) = Vno(1)

! left and right states
hl   = primL(1)     ;  hr   = primR(1)
uxl  = primL(2)     ;  uxr  = primR(2)
uyl  = primL(3)     ;  uyr  = primR(3)
pxxl = primL(4)     ;  pxxr = primR(4)
pxyl = primL(5)     ;  pxyr = primR(5)
pyyl = PrimL(6)     ;  pyyr = primR(6)

! coordinate transformation of u, v, P
ul = uxl*Vno(1) + uyl*Vno(2)
vl = uxl*Vmo(1) + uyl*Vmo(2)

Pnnl = Vno(1)*( Pxxl*Vno(1) + Pxyl*Vno(2) ) &
   & + Vno(2)*( Pxyl*Vno(1) + Pyyl*Vno(2) )

Pnml = Vmo(1)*( Pxxl*Vno(1) + Pxyl*Vno(2) ) &
   & + Vmo(2)*( Pxyl*Vno(1) + Pyyl*Vno(2) )

Pmml = Vmo(1)*( Pxxl*Vmo(1) + Pxyl*Vmo(2) ) &
   & + Vmo(2)*( Pxyl*Vmo(1) + Pyyl*Vmo(2) )

ur = uxr*Vno(1) + uyr*Vno(2)
vr = uxr*Vmo(1) + uyr*Vmo(2)

Pnnr = Vno(1)*( Pxxr*Vno(1) + Pxyr*Vno(2) ) &
   & + Vno(2)*( Pxyr*Vno(1) + Pyyr*Vno(2) )

Pnmr = Vmo(1)*( Pxxr*Vno(1) + Pxyr*Vno(2) ) &
   & + Vmo(2)*( Pxyr*Vno(1) + Pyyr*Vno(2) )

Pmmr = Vmo(1)*( Pxxr*Vmo(1) + Pxyr*Vmo(2) ) &
   & + Vmo(2)*( Pxyr*Vmo(1) + Pyyr*Vmo(2) )

! energy, pressure
El = 0.5d0 * ( ul*ul + vl*vl + g*hl + Pnnl + Pmml )
pl = hl * (0.5d0*g*hl + Pnnl*fact )
Cl = dsqrt(g*hl + 3.0d0*Pnnl )

Er = 0.5d0 * ( ur*ur + vr*vr + g*hr + Pnnr + Pmmr )
pr = hr * (0.5d0*g*hr + Pnnr*fact )
Cr = dsqrt(g*hr + 3.0d0*Pnnr )

! acoustic subsystem
sr = dmax1(ul+cl, ur+cr) ; sl = dmin1(ul-cl, ur-cr)

ml = hl*(ul-sl) ; mr = hr*(ur-sr)

uS  = ( ul*ml - ur*mr +    pl-pr    )/(ml-mr)
pS  = ( (ul-ur)*mr*ml + mr*pl-ml*pr )/(mr-ml)
ElS = El + (pl*ul-pS*uS)/ml
ErS = Er + (pr*ur-pS*uS)/mr

hlS   = ml/(uS-sl)
uls   = us
vls   = vl
pnnlS = pS/hlS - 0.5d0*g*HlS
pnmlS = pnml*hlS/hl
pmmlS = 2.0d0*ElS - g*hlS - pnnlS - ( Us**2 + vl**2)

hrS   = mr/(uS-sr)
urs   = us
vrs   = vr
pnnrS = pS/hrS - 0.5d0*g*HrS
pnmrS = pnmr*hrS/hr
pmmrS = 2.0d0*ErS - g*hrS - pnnrS - ( Us**2 + vr**2)

pmmlS = pmml
pmmrS = pmmr

uxls  = uls*Vno(1) + vls*Vmo(1)
uylS  = uls*Vno(2) + vls*Vmo(2)

pxxlS =   PnnlS*Vno(1)*Vno(1)                     &
        &  + PnmlS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
        &  + PmmlS*Vmo(1)*Vmo(1)

pxylS =   PnnlS*Vno(1)*Vno(2)                     &
        &  + PnmlS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
        &  + PmmlS*Vmo(1)*Vmo(2)

pyylS =   PnnlS*Vno(2)*Vno(2)                     &
        &  + PnmlS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
        &  + PmmlS*Vmo(2)*Vmo(2)

uxrs  = urs*Vno(1) + vrs*Vmo(1)
uyrS  = urs*Vno(2) + vrs*Vmo(2)

pxxrS =   PnnrS*Vno(1)*Vno(1)                     &
        &  + PnmrS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
        &  + PmmrS*Vmo(1)*Vmo(1)

pxyrS =   PnnrS*Vno(1)*Vno(2)                     &
        &  + PnmrS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
        &  + PmmrS*Vmo(1)*Vmo(2)

pyyrS =   PnnrS*Vno(2)*Vno(2)                     &
        &  + PnmrS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
        &  + PmmrS*Vmo(2)*Vmo(2)

Vpl  = (/hl ,  hl*uxl ,  hl*uyl , pxxl , pxyl ,  pyyl /)
VplS = (/hlS, hlS*uxlS, hlS*uylS, pxxlS, pxylS,  pyylS/)
VprS = (/hrS, hrS*uxrS, hrS*uyrS, pxxrS, pxyrS,  pyyrS/)
Vpr  = (/hr ,  hr*uxr ,  hr*uyr , pxxr , pxyr ,  pyyr /)

l1 = -dmin1(Sl, 0.0d0); l2 = -dmin1(uS, 0.0d0); l3 = -dmin1(Sr, 0.0d0)
r1 = dmax1(Sl, 0.0d0); r2 = dmax1(uS, 0.0d0); r3 = dmax1(Sr, 0.0d0)

fluxL(:) = fluxL(:) + l1 * (  Vpl(:) - VplS(:) ) &
       & + l2 * ( VplS(:) - VprS(:) ) &
       & + l3 * ( VprS(:) -  Vpr(:) )

fluxR(:) = fluxR(:) + r1 * (  Vpl(:) - VplS(:) ) &
       & + r2 * ( VplS(:) - VprS(:) ) &
       & + r3 * ( VprS(:) -  Vpr(:) )

! shear subsystem
sl = -dsqrt(pnnl); sr = dsqrt(pnnr)

vS    = ( hl*(pnml*fact- sl*vl) - hr*(pnmr*fact-sr*vr) )/(sr*hr - hl*sl)
hpnmS = (hr*hl)*( sr*pnml - sl*pnmr + sl*sr*(vr-vl) )/(sr*hr - hl*sl)
uS    = 0.0d0

hlS   = hl
ulS   = ul
vlS   = vs
pnnlS = pnnl
pnmlS = hpnmS/hlS
pmmlS = ( hpnmS**2 - hl**2*(pnml**2 - sl**2*pmml ) )/( (sl*hl)**2 )

hrS   = hr
urS   = ur
vrS   = vs
pnnrS = pnnr
pnmrS = hpnmS/hrS
pmmrS = ( hpnmS**2 - hr**2*(pnmr**2 - sr**2*pmmr ) )/( (sr*hr)**2 )

uxls  = uls*Vno(1) + vls*Vmo(1)
uylS  = uls*Vno(2) + vls*Vmo(2)

pxxlS = PnnlS*Vno(1)*Vno(1)                     &
   &  + PnmlS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
   &  + PmmlS*Vmo(1)*Vmo(1)

pxylS =   PnnlS*Vno(1)*Vno(2)                     &
   &  + PnmlS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
   &  + PmmlS*Vmo(1)*Vmo(2)

pyylS =   PnnlS*Vno(2)*Vno(2)                     &
   &  + PnmlS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
   &  + PmmlS*Vmo(2)*Vmo(2)

uxrs  = urs*Vno(1) + vrs*Vmo(1)
uyrS  = urs*Vno(2) + vrs*Vmo(2)

pxxrS = PnnrS*Vno(1)*Vno(1)                     &
   &  + PnmrS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
   &  + PmmrS*Vmo(1)*Vmo(1)

pxyrS =   PnnrS*Vno(1)*Vno(2)                     &
   &  + PnmrS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
   &  + PmmrS*Vmo(1)*Vmo(2)

pyyrS =   PnnrS*Vno(2)*Vno(2)                     &
   &  + PnmrS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
   &  + PmmrS*Vmo(2)*Vmo(2)

Vpl  = (/hl ,  hl*uxl ,  hl*uyl , pxxl , pxyl ,  pyyl /)
VplS = (/hlS, hlS*uxlS, hlS*uylS, pxxlS, pxylS,  pyylS/)
VprS = (/hrS, hrS*uxrS, hrS*uyrS, pxxrS, pxyrS,  pyyrS/)
Vpr  = (/hr ,  hr*uxr ,  hr*uyr , pxxr , pxyr ,  pyyr /)

l1 = -dmin1(Sl, 0.0d0); l2 = -dmin1(us, 0.0d0); l3 = -dmin1(Sr, 0.0d0)
r1 = dmax1(Sl, 0.0d0); r2 = dmax1(us, 0.0d0); r3 = dmax1(Sr, 0.0d0)

fluxL(:) = fluxL(:) + l1 * (  Vpl(:) - VplS(:) ) &
      &  + l2 * ( VplS(:) - VprS(:) ) &
      &  + l3 * ( VprS(:) -  Vpr(:) )

fluxR(:) = fluxR(:) + r1 * (  Vpl(:) - VplS(:) ) &
      &  + r2 * ( VplS(:) - VprS(:) ) &
      &  + r3 * ( VprS(:) -  Vpr(:) )

end subroutine riemann_solvers

subroutine reconstruct(ie, primL, primR)
use variables
implicit none
integer, intent(in) :: ie
real(kind=dp), intent(out) :: primL(1:6), primR(1:6)
real(kind=dp) :: consL(1:6), consR(1:6)
integer :: iv, c1, c2, v1, v2
real(kind=dp) :: du, gradv1, gradv2, mpc1(1:2), mpc2(1:2)
real(kind=dp), external :: minmod

v1 = face(1, ie)
v2 = face(2, ie)

c1 = cells_sur_iface(1, ie)
c2 = cells_sur_iface(2, ie)

mpc1 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c1) )/3.0
mpc2 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c2) )/3.0

do iv = 1, 6
   gradv1 = dot_product(grad(1:2, iv, c1), mpc1-cell_coord(:, c1))
   gradv2 = dot_product(grad(1:2, iv, c2), mpc2-cell_coord(:, c2))
   !if(iv .lt. 4 .and. sqrt(gradv1**2+gradv2**2) .gt. 1.d-8 ) print*, gradv1, gradv2
   !du = prim(iv, c2) - prim(iv, c1)
   !call limiter(du, gradv1, gradv2)
   ! no need for 0.5 as we are computing dr between edge midpoint and cell centers      
   !if(iv .lt. 4 .and. sqrt(gradv1**2+gradv2**2) .gt. 1.d-8 ) print*,'*', gradv1, gradv2
   consL(iv) = cons(iv, c1) + gradv1
   consR(iv) = cons(iv, c2) + gradv2 

   primL(1) = consL(1)
   primL(2) = consL(2)/consL(1)
   primL(3) = consL(3)/consL(1)
   primL(4) = consL(4)
   primL(5) = consL(5)
   primL(6) = consL(6)
   
   primR(1) = consR(1)
   primR(2) = consR(2)/consR(1)
   primR(3) = consR(3)/consR(1)
   primR(4) = consR(4)
   primR(5) = consR(5)
   primR(6) = consR(6)

enddo

end subroutine reconstruct

subroutine reconstruct_on_boundaries(ie, primL)
use variables
implicit none
integer, intent(in) :: ie
real(kind=dp), intent(out) :: primL(1:6)
integer :: iv, c1, v1, v2
real(kind=dp) :: gradv1, mpc1(1:2)
real(kind=dp), external :: minmod
!real(kind=dp) :: du

v1 = non_per_bface(1, ie)
v2 = non_per_bface(2, ie)
c1 = cells_sur_non_per_bface(1, ie)
mpc1 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c1) )/3.0
do iv = 1, 6
   gradv1 = dot_product(grad(1:2, iv, c1), mpc1-cell_coord(:, c1))
   !du = prim(iv, c2) - prim(iv, c1)
   !call limiter(du, gradv1, gradv2)
   primL(iv) = prim(iv, c1) + gradv1 
enddo

end subroutine reconstruct_on_boundaries

subroutine reconstruct_per_boundaries(ie, primL, primR)
use variables
implicit none
integer, intent(in) :: ie
real(kind=dp), intent(out) :: primL(1:6), primR(1:6)
integer :: iv, c1, c2, v1, v2
real(kind=dp) :: du, gradv1, gradv2, mpc1(1:2), mpc2(1:2)
real(kind=dp), external :: minmod

v1 = per_bface(1, ie)
v2 = per_bface(2, ie)

c1 = cells_sur_per_bface(1, ie)
c2 = cells_sur_per_bface(2, ie)

mpc1 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c1) )/3.0
mpc2 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c2) )/3.0

do iv = 1, 6
   gradv1 = dot_product(grad(1:2, iv, c1), mpc1-cell_coord(:, c1))
   gradv2 = dot_product(grad(1:2, iv, c2), mpc2-cell_coord(:, c2))

   du = prim(iv, c2) - prim(iv, c1)
   call limiter(du, gradv1, gradv2)
   ! no need for 0.5 as we are computing dr between edge midpoint and cell centers      
   primL(iv) = prim(iv, c1) + gradv1
   primR(iv) = prim(iv, c2) + gradv2

enddo

end subroutine reconstruct_per_boundaries

subroutine limiter(du, gradv1, gradv2)
use variables
implicit none
real(kind=dp), intent(in) :: du
real(kind=dp), intent(inout) :: gradv1, gradv2
real(kind=dp) :: gradm1, gradm2, fact
real(kind=dp) :: num, den, eps

  gradM1 = 2.0d0*gradv1 !- du 
  gradM2 = 2.0d0*gradv2 !- du
  fact = 1.0d0 ! use 2.0 for Superbee
  gradv1 = MinMod(fact*gradM1, du) ! DW sur ij/2
  gradv2 = MinMod(fact*gradM2, du) ! On a tjs |grad_lim| < |grad|

  !if(1==1)then

  !   eps = 1.0d-08
  !   IF(GradM1 * du > 0.0)THEN
  !      num = (du ** 2 + eps ** 2 ) * GradM1 + (GradM1 ** 2 + eps ** 2) * du
  !      den = ( GradM1 ** 2 +  du ** 2 + 2.0d0 * eps * eps )
  !      Gradv1 = 0.5d0 * num / den
  !   ELSE
  !      Gradv1 = 0.0d0
  !   END IF
  !   
  !   IF (GradM2 * du > 0.0d0)THEN
  !      num = (du ** 2 + eps ** 2 ) * GradM2 + (GradM2 ** 2 + eps ** 2) * du
  !      den = ( GradM2 ** 2 +  du ** 2 + 2.0d0 * eps * eps )
  !      Gradv2 = 0.5 * num / den
  !   ELSE
  !      Gradv2 = 0.0
  !   END IF
!
!  endif

contains
function MinMod(u, v) RESULT(mmod)
use variables
real(kind=dp), INTENT(IN) :: u, v
real(kind=dp) :: mmod, sngu, sngv
sngu = Sign(1.0d0, u)
sngv = Sign(1.0d0, v)
mmod = 0.25d0*dmin1(dabs(u), dabs( v)) * (sngu + sngv)
end function MinMod

end subroutine limiter

! 'Gradient Calculation Methods on Arbitrary Polyhedral Unstructured Meshes
! for Cell-Centered CFD Solvers', Emre Sozer, Christoph Brehm and Cetin C. Kiris
! https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20140011550.pdf

subroutine gradients(tg)
use variables
implicit none
integer :: ic, jc, c
real(kind=dp), intent(out) :: tg
real(kind=dp) :: dx, dy, wt, fact, h
real(kind=dp) :: a11, a12, a22, detA
real(kind=dp), dimension(1:6) :: dv, rhs1, rhs2
real(kind=dp) :: uij, uji
integer :: ie, j, v1, v2, c1, c2
real(kind=dp), dimension(1:2) :: mpc1, mpc2

grad = 0.0d0

if(trim(init_cond) == 'Exact2D')then

  fact = 1.0d0/(1.0d0 + (beta*tg)**2)      
  h = fact * H_0
  do ic = 1, nc
     grad(1:2, 1, ic) = 0.0d0
     grad(1, 2, ic) = beta * fact * beta * tg * h
     grad(2, 2, ic) = beta * fact * h
     grad(1, 3, ic) = -beta * fact * h
     grad(2, 3, ic) = beta * fact * beta * tg * h
     grad(1:2, 4:6, ic) = 0.0d0

  enddo      

else        

  ! loop over no of cells
  do ic = 1, nc
     a11  = 0.0d0 ; a12  = 0.0d0 ; a22 = 0.0d0
     rhs1 = 0.0d0 ; rhs2 = 0.0d0
     ! loop over cells surrounding cells

     do c  = csuc2(ic)+1, csuc2(ic+1)
        jc = csuc1(c)

        dx = cell_coord(1, jc) - cell_coord(1, ic)
        dy = cell_coord(2, jc) - cell_coord(2, ic)
        wt = 1.0d0/dsqrt(dx*dx + dy*dy)

        a11 = a11 + wt*dx*dx
        a12 = a12 + wt*dx*dy
        a22 = a22 + wt*dy*dy

        dv = cons(1:6, jc) - cons(1:6, ic)
        rhs1 = rhs1 + wt*dx*dv
        rhs2 = rhs2 + wt*dy*dv
     enddo
     detA = a11*a22 - a12*a12
     if(dabs(detA) .lt. 1.d-10)then
        print*, 'determinent became zero in gradient computation at : ', ic, detA
        call abort
     endif
     grad(1, :, ic) = (rhs1(:)*a22 - rhs2(:)*a12)/detA
     grad(2, :, ic) = (rhs2(:)*a11 - rhs1(:)*a12)/detA
  enddo

endif

grad(1:2, 4, :) = 0.0d0
grad(1:2, 5, :) = 0.0d0
grad(1:2, 6, :) = 0.0d0

do ie = 1, nif
   v1 = face(1, ie)
   v2 = face(2, ie)
   c1 = cells_sur_iface(1, ie)
   c2 = cells_sur_iface(2, ie)

   mpc1 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c1) )/3.0
   mpc2 = (coord(:, v2) + coord(:, v1) + cell_coord(:, c2) )/3.0

   do j = 1, 3
      uij = cons(j,c1) + dot_product(grad(:, j, c1), mpc1-cell_coord(:,c1) )
      uji = cons(j,c2) + dot_product(grad(:, j, c2), mpc2-cell_coord(:,c2) )

      if( (uij .gt. dmax1(cons(j,c1), cons(j,c2))) .or. (uij .lt. dmin1(cons(j,c1), cons(j,c2))) ) grad(:, j, c1) = 0.0d0
      if( (uji .gt. dmax1(cons(j,c1), cons(j,c2))) .or. (uji .lt. dmin1(cons(j,c1), cons(j,c2))) ) grad(:, j, c2) = 0.0d0
   enddo
enddo

end subroutine gradients

subroutine prim_to_cons()
use variables
implicit none
integer :: i

do i = 1, nc
   cons(1, i) = prim(1, i)
   cons(2, i) = prim(1, i) * prim(2, i)
   cons(3, i) = prim(1, i) * prim(3, i)
   cons(4, i) = prim(4, i)
   cons(5, i) = prim(5, i)
   cons(6, i) = prim(6, i)
enddo

end subroutine prim_to_cons

subroutine cons_to_prim()
use variables
implicit none
integer :: i
real(kind=dp) :: eps = 1.d-14

do i = 1, nc
   prim(1, i) = cons(1, i)
   prim(2 ,i) = cons(2 ,i)/cons(1, i)
   prim(3, i) = cons(3, i)/cons(1, i)
   prim(4, i) = cons(4, i)
   prim(5, i) = cons(5, i)
   prim(6, i) = cons(6, i)

   prim(4, i) = cons(4, i)
   prim(4, i) = dmax1(prim(4, i), eps)

   prim(6, i) = cons(6, i)
   prim(6, i) = dmax1(prim(6, i), eps)

   prim(5, i) = cons(5, i)
   prim(5, i) = dmin1( prim(5, i),  dsqrt(prim(4, i) * prim(6, i)) )
   prim(5, i) = dmax1( prim(5, i), -dsqrt(prim(4, i) * prim(6, i)) )
      
   cons(4:6, i) = prim(4:6, i)

   prim(7, i) = 0.5d0*( g*prim(1, i) + prim(4, i) + prim(6, i) )
   prim(8, i) = dsqrt ( g*prim(1, i) + 3.0d0*dmax1(prim(4, i), prim(6, i)) )
   prim(9, i) = dsqrt ( dmax1(prim(4, i), prim(6, i)))
   prim(10, i) = 0.5d0*g*(prim(1, i)*prim(1, i)) + dmax1(prim(4, i), prim(6, i)) * prim(1, i)
enddo

end subroutine cons_to_prim


subroutine source(cell_id, src)
use variables
implicit none
integer, intent(in) :: cell_id
real(kind=dp), intent(out) :: src(1:6)
real(kind=dp) :: hc, uc, vc, pxxc, pxyc, pyyc
real(kind=dp) :: norm_u, traceP, alpha !, jac_term

if (cons(1, cell_id) .le. 1.d-8) then
    print*, 'Error in source term: ', cons(1, cell_id), cell_id
end if

hc   = prim(1, cell_id)
uc   = prim(2, cell_id)
vc   = prim(3, cell_id)
pxxc = prim(4, cell_id)
pxyc = prim(5, cell_id)
pyyc = prim(6, cell_id)

traceP = pxxc + pyyc
norm_u = dsqrt(uc**2 + vc**2)
alpha = cr0 * (traceP - phi*hc*hc )/max(traceP*traceP, 1.0E-19)
alpha = dmax1(alpha, 0.0d0)

src(1) = 0.0d0
src(2) = - cf * norm_u * uc  + g * dtan(angle) * hc
src(3) = - cf * norm_u * vc
src(4) = - 2.0d0 * alpha * pxxc * norm_u**3 / hc
src(5) = - 2.0d0 * alpha * pxyc * norm_u**3 / hc
src(6) = - 2.0d0 * alpha * pyyc * norm_u**3 / hc

end subroutine source

end module solver
