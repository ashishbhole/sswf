subroutine preprocessor()
use variables
use preproc
implicit none
print*, "!------------------------------------------"
print*, "Preprocessing the mesh"

call cell_area()
call cell_center()
call cells_surrounding_point()
call list_of_faces()
call cells_surrounding_internal_face()
call cells_surrounding_non_per_bface()
call calculate_characteristic_length()
if(trim(per_dir) == 'x' .or. trim(per_dir) == 'y' .or. trim(per_dir) == 'xy')then
   call periodic_faces()
   call cells_surrounding_per_bface()
endif

call cells_on_boundaries()
!call check_if_cells_outward_normals_close()

if(trim(recon) == 'second')then
   if(trim(csuc_type) == 'regular')then
      call cells_surrounding_cell()
   elseif(trim(csuc_type) == 'extended')then
      call extended_cells_surrounding_cell()
   else
      print*, 'specify valid flag for cells surroung cells: regular or extended'
      call abort
   endif
endif

print*, "!------------------------------------------"
end subroutine preprocessor
