module variables
implicit none

integer, parameter :: dp=kind(1.0d0)

logical :: periodic, restart
character(len=64) :: gridfile, init_cond, bound_cond, per_dir, csuc_type, recon
real(kind=dp)  :: Lx, Ly, T_final, cfl, lbeta
real(kind=dp)  :: angle, H_0, phi, cf, cr0, amp, lambda, gamma, beta
integer :: it_max, it_save

integer :: nve(1:3) = (/2, 3, 4/)
integer :: face_in_cell(1:2, 1:4, 2:3)
integer :: nv, nelem, ntags, nc, ntria, nquad, n_pnodes, nif, ncg
integer :: x_per_tag1, x_per_tag2, y_per_tag1, y_per_tag2
integer, allocatable :: telem(:), tface(:), tcell(:), cell(:,:), bvert_flag(:)
integer, allocatable :: tag_list(:), face(:,:), pface_list(:)
integer, allocatable :: csuc1(:), csuc2(:)
integer, allocatable :: csup1(:), csup2(:)
real(kind=dp), allocatable :: coord(:,:), carea(:), cell_coord(:, :)
real(kind=dp) :: char_len

integer :: per_nbf, non_per_nbf
integer, allocatable :: per_btag(:), non_per_btag(:)
integer, allocatable :: per_bface(:,:), non_per_bface(:,:)
integer, allocatable :: cells_sur_iface(:, :), cells_sur_per_bface(:, :), cells_sur_non_per_bface(:, :)
real(kind=dp), allocatable :: anorm_internal(:, :), anorm_non_per(:, :), anorm_per(:, :)
integer, allocatable :: cob(:)
real(kind=dp), allocatable :: prim(:,:), cons0(:,:), cons(:, :), res(:, :), grad(:, :, :), k1(:,:)
real(kind=dp), allocatable :: cons1(:,:), cons2(:,:)

integer :: nrk

real(kind=dp), parameter :: g  = 9.81d0
real(kind=dp), parameter :: pi = 4.0d0*atan(1.0d0)

integer :: it
real(kind=dp) :: time, dt

end module variables
