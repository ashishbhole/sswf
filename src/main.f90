program main
use variables
use preproc
use auxillary_conditions
use solver
use postproc
implicit none
integer :: i
character(len = 32) :: arg, input_filename
real(kind=dp) :: res_norm, tstage, speed
real(kind=dp), dimension(1:6) :: src

namelist /input_list/ gridfile, restart, periodic, per_dir, &
                    & x_per_tag1, x_per_tag2, y_per_tag1, y_per_tag2, Lx, Ly, &
                    & init_cond, bound_cond, angle, T_final, cfl, it_max, it_save, &
                    & H_0, phi, cf, cr0, amp, lambda, gamma, beta, csuc_type, recon, lbeta, nrk

if(iargc() .ne. 1) then
    print*, "Please type : ./sswf input_file_name"
    stop
endif
DO i = 1, iargc()
  CALL getarg(i, arg)
  if(i == 1) input_filename = trim(arg)
enddo
            
write(*,*) " reading input file"
open(1, file=trim(input_filename))
  read(1, input_list)
close(1)
write(*,*) " reading input file complete"

call read_gmsh_grid()
call preprocessor()

allocate(cons0(1:6, 1:nc), cons(1:6, 1:nc))
allocate(prim(1:10, 1:ncg), res(1:6, 1:ncg))
allocate(grad(1:2, 1:6, 1:nc))
allocate(k1(1:6, 1:nc))
allocate(cons1(1:6, 1:nc), cons2(1:6, 1:nc))

if(restart)then
   call read_restart_file()
   print*, 'restarting solution from time and it = ', real(time), it   
   call prim_to_cons()
   call cons_to_prim()   
else
   it = 0; time = 0.0d0     
   call initial_condition()
   call prim_to_cons()
   call cons_to_prim()
   !call boundary_condition(time)
   call system('rm -rf output')
   call system('mkdir output')
   call save_solution()
endif

! select reconstruction scheme
if(trim(recon) == 'first')then
   print*, "reconstruction : first order"
elseif(trim(recon) == 'second')then
   print*, "reconstruction : second order"
else
   print*, "enter valid reconstruction type: first or second"
   call abort
endif

call system('rm -r entropy_time_evolution.txt')

open(100,file='time_series.txt')
do while ((it .le. it_max) .and. (time .lt. t_final))

   ! calculate time step from cfl
   dt = 1.d10
   do i = 1, nc
      speed = dmax1( dabs(prim(2,i)+prim(8,i)) , dabs(prim(2,i)-prim(8,i)) )
      dt = dmin1(dt, cfl * char_len / speed)
   enddo
   dt = dmin1(dt, t_final - time)

   if(nrk==1)then
      call cons_to_prim()
      tstage = time      
      if(trim(recon) == 'second') call gradients(tstage)
      res = 0.0d0
      call calculate_residue()
      call boundary_condition(tstage)
      do i = 1, nc
         cons(:,i) = cons(:,i) - dt*res(:,i)/carea(i)
      end do
   elseif(nrk==2)then
      cons0 = cons 
      call cons_to_prim()
      tstage = time
      if(trim(recon) == 'second') call gradients(tstage)
      res = 0.0d0; k1 = 0
      call calculate_residue()
      call boundary_condition(tstage)
      k1 = res
      do i = 1, nc
         cons(:,i) = cons0(:,i) - dt*k1(:,i)/carea(i)
      end do
      cons1 = cons

      call cons_to_prim()
      tstage = time + dt
      if(trim(recon) == 'second') call gradients(tstage)
      res = 0.0d0
      call calculate_residue()
      call boundary_condition(tstage)
      do i = 1, nc
         cons(:,i) = cons0(:,i) - 0.5d0*dt*(k1(:,i) + res(:,i))/carea(i)
      end do
      !cons2 = cons
      !do i = 1, nc
      !   cons(:,i) = 0.5d0*(cons1(:,i) + cons2(:,i))
      !end do
   else
      print*,'use nrk = 1 or 2'
      call abort
   endif

   if((trim(init_cond) == 'Roll1D') .or. (trim(init_cond) == 'Roll2D'))then
      call cons_to_prim()
      do i = 1, nc
         call source(i, src)
         cons(:, i) = cons(:, i) + dt*src
      enddo
   endif

   call cons_to_prim()

   it = it + 1 ; time = time + dt

   res_norm = 0.0d0
   do i = 1, nc
      res_norm = res_norm + res(1, i)*res(1, i)
   enddo
   write(100, *) real(time), real(dt)
   print*, it, real(dt), real(time), real(res_norm)

   if(isnan(res_norm))then
      print*, 'code blown up'
      call abort
   endif

   if(mod(it, it_save) == 0) call save_solution()

   call total_entropy_time_evolution()
enddo

close(100)

if(trim(init_cond) == 'Exact2D')then
   call calculate_error()
endif

call save_solution()

end program main
