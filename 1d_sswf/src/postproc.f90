module postproc
contains
subroutine save_solution()
use variables
implicit none
integer, parameter :: fid = 10
integer :: i
character(64) :: filename, filename1
print*, 'Saving solution at time and iteration : ', real(time), it
write(filename, '(a,i7.7,a)') './output/solution_', it, '.vtk'
open(fid, file = trim(filename))
do i = 1, nc
   write(fid,*) real(coord(i)), real(prim(1:6, i))
enddo
close(fid)

write(filename1, '(a)') './output/solution_restart.vtk'
call system('cp '//filename//' '//filename1)

end subroutine save_solution
end module postproc
