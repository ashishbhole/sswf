module solver
contains

subroutine calculate_residue()
use variables
implicit none
integer :: ie, c1, c2
real(kind=dp) :: primLL(1:6), primL(1:6), primR(1:6), fluxL(1:6), fluxR(1:6), anorm(1:2)

! loop over all internal faces (edges in 2D)
do ie = 0, nc
   c1 = ie ; c2 = ie+1
   fluxL = 0.0d0 ; fluxR = 0.0d0 
   
   anorm = (/1.0d0, 0.0d0/)
   if(trim(method) == 'first')then
      primL(1:6) = prim(1:6, c1) ; primR(1:6) = prim(1:6, c2)   
      call riemann_solvers(anorm, primL, primR, fluxL, fluxR)
      res(1:6, c1) = res(1:6, c1) - fluxL(1:6)
      res(1:6, c2) = res(1:6, c2) + fluxR(1:6) 
   elseif(trim(method) == 'second')then
      PrimLL =  Prim(1:6,c1) - 0.5d0*dx*Grad(1:6,c1)
      PrimL  =  Prim(1:6,c1) + 0.5d0*dx*Grad(1:6,c1)
      PrimR  =  Prim(1:6,c2) - 0.5d0*dx*Grad(1:6,c2)
      call riemann_solvers(anorm, primL, primR, fluxL, fluxR)
      res(1:6, c1) = res(1:6, c1) - fluxL(1:6)
      res(1:6, c2) = res(1:6, c2) + fluxR(1:6) 
      call riemann_solvers(anorm, primLL, primL, fluxL, fluxR)
      res(1:6, c1) = res(1:6, c1) - 0.5d0*(fluxL(1:6) - fluxR(1:6))
   endif

enddo
end subroutine calculate_residue   

! Originally witten by Prof. Boniface Nkonga
subroutine riemann_solvers(V, primL, primR, fluxL, fluxR)
use variables
implicit none
real(kind=dp), intent(in) :: V(1:2), primL(1:6), primR(1:6)
real(kind=dp), intent(out) :: fluxL(1:6), fluxR(1:6)

real(kind=dp) :: Vno(1:2), Vmo(1:2)
real(kind=dp) :: hl, uxl, uyl, pxxl, pxyl, pyyl, cl, ml, sl, pl
real(kind=dp) :: hr, uxr, uyr, pxxr, pxyr, pyyr, cr, mr, sr, pr
real(kind=dp) :: ul, vl, ur, vr, Pnnl, Pnml, Pmml, Pnnr, Pnmr, Pmmr
real(kind=dp) :: hlS, uxlS, uylS, pxxlS, pxylS, pyylS
real(kind=dp) :: hrS, uxrS, uyrS, pxxrS, pxyrS, pyyrS
real(kind=dp) :: ulS, vlS, pnnlS, pnmlS, pmmlS
real(kind=dp) :: urS, vrS, pnnrS, pnmrS, pmmrS
real(kind=dp) :: pS, uS, ElS, ErS, El, Er, vS, hpnmS
real(kind=dp) :: l1, l2, l3, r1, r2, r3
real(kind=dp) :: Vpl(1:6), VplS(1:6), VprS(1:6), Vpr(1:6)
real(kind=dp) :: fact = 1.0d0

! unit normal to the edge
Vno = V/dsqrt(V(1)**2 + V(2)**2) 
!Vmo(1) = Vno(2) ; Vmo(2) = - Vno(1)
Vmo(1) = -Vno(2) ; Vmo(2) = Vno(1)

! left and right states
hl   = primL(1)     ;  hr   = primR(1)
uxl  = primL(2)     ;  uxr  = primR(2)
uyl  = primL(3)     ;  uyr  = primR(3)
pxxl = primL(4)     ;  pxxr = primR(4)
pxyl = primL(5)     ;  pxyr = primR(5)
pyyl = PrimL(6)     ;  pyyr = primR(6)

! coordinate transformation of u, v, P
ul = uxl*Vno(1) + uyl*Vno(2)
vl = uxl*Vmo(1) + uyl*Vmo(2)

Pnnl = Vno(1)*( Pxxl*Vno(1) + Pxyl*Vno(2) ) &
   & + Vno(2)*( Pxyl*Vno(1) + Pyyl*Vno(2) )

Pnml = Vmo(1)*( Pxxl*Vno(1) + Pxyl*Vno(2) ) &
   & + Vmo(2)*( Pxyl*Vno(1) + Pyyl*Vno(2) )

Pmml = Vmo(1)*( Pxxl*Vmo(1) + Pxyl*Vmo(2) ) &
   & + Vmo(2)*( Pxyl*Vmo(1) + Pyyl*Vmo(2) )

ur = uxr*Vno(1) + uyr*Vno(2)
vr = uxr*Vmo(1) + uyr*Vmo(2)

Pnnr = Vno(1)*( Pxxr*Vno(1) + Pxyr*Vno(2) ) &
   & + Vno(2)*( Pxyr*Vno(1) + Pyyr*Vno(2) )

Pnmr = Vmo(1)*( Pxxr*Vno(1) + Pxyr*Vno(2) ) &
   & + Vmo(2)*( Pxyr*Vno(1) + Pyyr*Vno(2) )

Pmmr = Vmo(1)*( Pxxr*Vmo(1) + Pxyr*Vmo(2) ) &
   & + Vmo(2)*( Pxyr*Vmo(1) + Pyyr*Vmo(2) )

! energy, pressure
El = 0.5d0 * ( ul*ul + vl*vl + g*hl + Pnnl + Pmml )
pl = hl * (0.5d0*g*hl + Pnnl*fact )
Cl = dsqrt(g*hl + 3.0d0*Pnnl )

Er = 0.5d0 * ( ur*ur + vr*vr + g*hr + Pnnr + Pmmr )
pr = hr * (0.5d0*g*hr + Pnnr*fact )
Cr = dsqrt(g*hr + 3.0d0*Pnnr )

! acoustic subsystem
sr = dmax1(ul+cl, ur+cr) ; sl = dmin1(ul-cl, ur-cr)

ml = hl*(ul-sl) ; mr = hr*(ur-sr)

uS  = ( ul*ml - ur*mr +    pl-pr    )/(ml-mr)
pS  = ( (ul-ur)*mr*ml + mr*pl-ml*pr )/(mr-ml)
ElS = El + (pl*ul-pS*uS)/ml
ErS = Er + (pr*ur-pS*uS)/mr

hlS   = ml/(uS-sl)
uls   = us
vls   = vl
pnnlS = pS/hlS - 0.5d0*g*HlS
pnmlS = pnml*hlS/hl
pmmlS = 2.0d0*ElS - g*hlS - pnnlS - ( Us**2 + vl**2)

hrS   = mr/(uS-sr)
urs   = us
vrs   = vr
pnnrS = pS/hrS - 0.5d0*g*HrS
pnmrS = pnmr*hrS/hr
pmmrS = 2.0d0*ErS - g*hrS - pnnrS - ( Us**2 + vr**2)

pmmlS = pmml
pmmrS = pmmr

uxls  = uls*Vno(1) + vls*Vmo(1)
uylS  = uls*Vno(2) + vls*Vmo(2)

pxxlS =   PnnlS*Vno(1)*Vno(1)                     &
        &  + PnmlS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
        &  + PmmlS*Vmo(1)*Vmo(1)

pxylS =   PnnlS*Vno(1)*Vno(2)                     &
        &  + PnmlS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
        &  + PmmlS*Vmo(1)*Vmo(2)

pyylS =   PnnlS*Vno(2)*Vno(2)                     &
        &  + PnmlS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
        &  + PmmlS*Vmo(2)*Vmo(2)

uxrs  = urs*Vno(1) + vrs*Vmo(1)
uyrS  = urs*Vno(2) + vrs*Vmo(2)

pxxrS =   PnnrS*Vno(1)*Vno(1)                     &
        &  + PnmrS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
        &  + PmmrS*Vmo(1)*Vmo(1)

pxyrS =   PnnrS*Vno(1)*Vno(2)                     &
        &  + PnmrS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
        &  + PmmrS*Vmo(1)*Vmo(2)

pyyrS =   PnnrS*Vno(2)*Vno(2)                     &
        &  + PnmrS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
        &  + PmmrS*Vmo(2)*Vmo(2)

Vpl  = (/hl ,  hl*uxl ,  hl*uyl , pxxl , pxyl ,  pyyl /)
VplS = (/hlS, hlS*uxlS, hlS*uylS, pxxlS, pxylS,  pyylS/)
VprS = (/hrS, hrS*uxrS, hrS*uyrS, pxxrS, pxyrS,  pyyrS/)
Vpr  = (/hr ,  hr*uxr ,  hr*uyr , pxxr , pxyr ,  pyyr /)

l1 = -dmin1(Sl, 0.0d0); l2 = -dmin1(uS, 0.0d0); l3 = -dmin1(Sr, 0.0d0)
r1 = dmax1(Sl, 0.0d0); r2 = dmax1(uS, 0.0d0); r3 = dmax1(Sr, 0.0d0)

fluxL(:) = fluxL(:) + l1 * (  Vpl(:) - VplS(:) ) &
       & + l2 * ( VplS(:) - VprS(:) ) &
       & + l3 * ( VprS(:) -  Vpr(:) )

fluxR(:) = fluxR(:) + r1 * (  Vpl(:) - VplS(:) ) &
       & + r2 * ( VplS(:) - VprS(:) ) &
       & + r3 * ( VprS(:) -  Vpr(:) )

! shear subsystem
sl = -dsqrt(pnnl); sr = dsqrt(pnnr)

vS    = ( hl*(pnml*fact- sl*vl) - hr*(pnmr*fact-sr*vr) )/(sr*hr - hl*sl)
hpnmS = (hr*hl)*( sr*pnml - sl*pnmr + sl*sr*(vr-vl) )/(sr*hr - hl*sl)
uS    = 0.0d0

hlS   = hl
ulS   = ul
vlS   = vs
pnnlS = pnnl
pnmlS = hpnmS/hlS
pmmlS = ( hpnmS**2 - hl**2*(pnml**2 - sl**2*pmml ) )/( (sl*hl)**2 )

hrS   = hr
urS   = ur
vrS   = vs
pnnrS = pnnr
pnmrS = hpnmS/hrS
pmmrS = ( hpnmS**2 - hr**2*(pnmr**2 - sr**2*pmmr ) )/( (sr*hr)**2 )

uxls  = uls*Vno(1) + vls*Vmo(1)
uylS  = uls*Vno(2) + vls*Vmo(2)

pxxlS = PnnlS*Vno(1)*Vno(1)                     &
   &  + PnmlS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
   &  + PmmlS*Vmo(1)*Vmo(1)

pxylS =   PnnlS*Vno(1)*Vno(2)                     &
   &  + PnmlS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
   &  + PmmlS*Vmo(1)*Vmo(2)

pyylS =   PnnlS*Vno(2)*Vno(2)                     &
   &  + PnmlS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
   &  + PmmlS*Vmo(2)*Vmo(2)

uxrs  = urs*Vno(1) + vrs*Vmo(1)
uyrS  = urs*Vno(2) + vrs*Vmo(2)

pxxrS = PnnrS*Vno(1)*Vno(1)                     &
   &  + PnmrS*( Vno(1)*Vmo(1) + Vmo(1)*Vno(1) ) &
   &  + PmmrS*Vmo(1)*Vmo(1)

pxyrS =   PnnrS*Vno(1)*Vno(2)                     &
   &  + PnmrS*( Vno(1)*Vmo(2) + Vmo(1)*Vno(2) ) &
   &  + PmmrS*Vmo(1)*Vmo(2)

pyyrS =   PnnrS*Vno(2)*Vno(2)                     &
   &  + PnmrS*( Vno(2)*Vmo(2) + Vmo(2)*Vno(2) ) &
   &  + PmmrS*Vmo(2)*Vmo(2)

Vpl  = (/hl ,  hl*uxl ,  hl*uyl , pxxl , pxyl ,  pyyl /)
VplS = (/hlS, hlS*uxlS, hlS*uylS, pxxlS, pxylS,  pyylS/)
VprS = (/hrS, hrS*uxrS, hrS*uyrS, pxxrS, pxyrS,  pyyrS/)
Vpr  = (/hr ,  hr*uxr ,  hr*uyr , pxxr , pxyr ,  pyyr /)

l1 = -dmin1(Sl, 0.0d0); l2 = -dmin1(us, 0.0d0); l3 = -dmin1(Sr, 0.0d0)
r1 = dmax1(Sl, 0.0d0); r2 = dmax1(us, 0.0d0); r3 = dmax1(Sr, 0.0d0)

fluxL(:) = fluxL(:) + l1 * (  Vpl(:) - VplS(:) ) &
      &  + l2 * ( VplS(:) - VprS(:) ) &
      &  + l3 * ( VprS(:) -  Vpr(:) )

fluxR(:) = fluxR(:) + r1 * (  Vpl(:) - VplS(:) ) &
      &  + r2 * ( VplS(:) - VprS(:) ) &
      &  + r3 * ( VprS(:) -  Vpr(:) )

end subroutine riemann_solvers

subroutine prim_to_cons()
use variables
implicit none
integer :: i
do i = 1, nc
   cons(1, i) = prim(1, i)
   cons(2, i) = prim(1, i) * prim(2, i)
   cons(3, i) = prim(1, i) * prim(3, i)
   cons(4, i) = prim(4, i)
   cons(5, i) = prim(5, i)
   cons(6, i) = prim(6, i)
enddo
end subroutine prim_to_cons

subroutine cons_to_prim()
use variables
implicit none
integer :: i
real(kind=dp) :: eps = 1.d-14
do i = 1, nc
   prim(1, i) = cons(1, i)
   prim(2 ,i) = cons(2 ,i)/cons(1, i)
   prim(3, i) = cons(3, i)/cons(1, i)
   prim(4, i) = cons(4, i)
   prim(5, i) = cons(5, i)
   prim(6, i) = cons(6, i)

   prim(4, i) = cons(4, i)
   prim(4, i) = dmax1(prim(4, i), eps)

   prim(6, i) = cons(6, i)
   prim(6, i) = dmax1(prim(6, i), eps)

   prim(5, i) = cons(5, i)
   prim(5, i) = dmin1( prim(5, i),  dsqrt(prim(4, i) * prim(6, i)) )
   prim(5, i) = dmax1( prim(5, i), -dsqrt(prim(4, i) * prim(6, i)) )
      
   cons(4:6, i) = prim(4:6, i)

   prim(7, i) = 0.5d0*( g*prim(1, i) + prim(4, i) + prim(6, i) )
   prim(8, i) = dsqrt ( g*prim(1, i) + 3.0d0*dmax1(prim(4, i), prim(6, i)) )
   prim(9, i) = dsqrt ( dmax1(prim(4, i), prim(6, i)))
   prim(10, i) = 0.5d0*g*(prim(1, i)*prim(1, i)) + dmax1(prim(4, i), prim(6, i)) * prim(1, i)
enddo
end subroutine cons_to_prim

Subroutine MinMod(Grad1, Grad2, Gradc)
use variables        
REAL (KIND = DP), DIMENSION(:), INTENT(IN)   :: Grad1, Grad2
REAL (KIND = DP), DIMENSION(:), INTENT(OUT)  :: Gradc
INTEGER :: iv

DO iv = 1, 6
   if( Grad1(iv)*Grad2(iv) < 0 ) Then
      Gradc(iv)  = 0
   ELSE IF( Grad1(iv) > 0 ) Then
      Gradc(iv) = MIN( Grad1(iv), Grad2(iv) )
   ELSE IF( Grad1(iv) <= 0 ) Then
      Gradc(iv) = MAX( Grad1(iv), Grad2(iv) )
   ELSE
      Gradc(iv)  = 0
   END IF

   !if(Grad1(iv)*Grad2(iv) < 0 ) Then
   !   Gradc(iv)  = 0
   !else
   !   if(abs(Grad1(iv)) .lt. abs(Grad2(iv)))then
   !      Gradc(iv)  = Grad1(iv)     
   !   elseif(abs(Grad1(iv)) .gt. abs(Grad2(iv)))then
   !      Gradc(iv)  = Grad2(iv)     
   !  else
   !      Gradc(iv)  = 0     
   !   endif
   !endif

END DO
if(trim(init_cond)=='Shear1D')then
!Gradc(4) = 0.0d0
!Gradc(5) = 0.0d0
!Gradc(6) = 0.0d0
else
Gradc(4) = 0.0d0
Gradc(5) = 0.0d0
Gradc(6) = 0.0d0        
endif
end Subroutine MinMod


Subroutine limiter(Gl, Gr, Gradc)
use variables
integer :: iv
real (kind = dp), dimension(:), intent(in)   :: Gl, Gr
real (kind = dp), dimension(:), intent(out)  :: Gradc
real (kind = dp) :: ratio, betaL, betaR, xiL, xiR

betaL = 2.0d0/(1.0d0 + cfl)
betaR = 2.0d0/(1.0d0 - cfl)

do iv = 1, 6

   ratio = Gl(iv)/(Gr(iv) + 1.d-14)    
   xiL = 2.0d0*betaL*ratio / (1.0d0 + ratio)
   xiR = 2.0d0*betaR / (1.0d0 + ratio)

   if(ratio .lt. 0.0d0)then 
      gradc(iv) = 0.0d0
   else
      !gradc(iv) = dmin1(xiL, xiR)     
      !gradc(iv) = dmin1(ratio*(1.0d0+ratio)/(1.0d0+ratio**2), xiR) ! vanalbada
      gradc(iv) = dmin1(2.0d0*ratio/(1.0d0+ratio**2), xiR) ! vanleer
      !gradc(iv) = dmin1(2.0d0/(1.0d0+ratio), 2.0d0*ratio/(1.0d0+ratio) ) ! minmod
      !gradc(iv) = dmin1( dmin1(1.0d0, 4.0d0*ratio/(1.0d0+ratio)),  4.0d0*ratio/(1.0d0+ratio) ) ! bj
   endif 

enddo

if(trim(init_cond)=='Shear1D')then
!Gradc(4) = 0.0d0
!Gradc(5) = 0.0d0
!Gradc(6) = 0.0d0
else
!Gradc(4) = 0.0d0
!Gradc(5) = 0.0d0
!Gradc(6) = 0.0d0
endif

end Subroutine limiter


Subroutine G_Grad_X()
use variables
integer :: ix, ixl, ixr, ixll, j
real(kind=dp), DIMENSION(6) :: Gl, Gr, Gradc, ratio

do ix = 1, nc -1
   ixl   = ix
   ixr   = ix+1
   ixll  = max(ix-1, 0)
   Gl    = (Prim(1:6,ixl)- Prim(1:6,ixll))/dx
   Gr    = (Prim(1:6,ixr)- Prim(1:6,ixl ))/dx
   !call MinMod(Gl, Gr, Gradc)
   call limiter(Gl, Gr, Gradc)
   do j = 1, 6
      Grad(j, ix) = Gradc(j)* 0.5d0*(Gl(j) + Gr(j))
   enddo
enddo

if(trim(init_cond)=='Roll1D')then
   Gl    = (Prim(1:6,1)- Prim(1:6,nc-1))/dx
   Gr    = (Prim(1:6,2)- Prim(1:6,1))/dx
   !call MinMod(Gl, Gr, Gradc)
   !Grad(1:6, 1) = Gradc
   call limiter(Gl, Gr, Gradc)
   do j = 1, 6
      Grad(j, 1) = Gradc(j)* 0.5d0*(Gl(j) + Gr(j))
   enddo   
   Grad(1:6, nc) = Grad(1:6, 1)

   grad(1:6, 0) = grad(1:6, nc-1)
   grad(1:6, nc+1) = grad(1:6, 2)   
endif

end subroutine G_Grad_X

subroutine source(cell_id, src)
use variables
implicit none
integer, intent(in) :: cell_id
real(kind=dp), intent(out) :: src(1:6)
real(kind=dp) :: hc, uc, vc, pxxc, pxyc, pyyc
real(kind=dp) :: norm_u, traceP, alpha !, jac_term

if (cons(1, cell_id) .le. 1.d-8) then
    print*, 'Error in source term: ', cons(1, cell_id), cell_id
end if

hc   = prim(1, cell_id)
uc   = prim(2, cell_id)
vc   = prim(3, cell_id)
pxxc = prim(4, cell_id)
pxyc = prim(5, cell_id)
pyyc = prim(6, cell_id)

traceP = pxxc + pyyc
norm_u = dsqrt(uc**2 + vc**2)
alpha = cr0 * (traceP - phi*hc*hc )/max(traceP*traceP, 1.0E-19)
alpha = dmax1(alpha, 0.0d0)

src(1) = 0.0d0
src(2) = - cf * norm_u * uc  + g * dtan(angle) * hc
src(3) = - cf * norm_u * vc
src(4) = - 2.0d0 * alpha * pxxc * norm_u**3 / hc
src(5) = - 2.0d0 * alpha * pxyc * norm_u**3 / hc
src(6) = - 2.0d0 * alpha * pyyc * norm_u**3 / hc

end subroutine source


end module solver
