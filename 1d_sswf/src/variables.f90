module variables
implicit none
integer :: nc, it, it_max, it_save
integer, parameter :: dp=kind(1.0d0)
real(kind=dp), parameter :: g  = 9.81d0
real(kind=dp), parameter :: pi = 4.0d0*atan(1.0d0)
character(len = 32) :: method, init_cond
real(kind=dp) :: res_norm, tstage, speed
real(kind=dp)  :: T_final, cfl, time, dt, dx, Lx
real(kind=dp)  :: H_0, phi, cf, cr0, amp, m, U_0, angle
real(kind=dp), allocatable :: coord(:), cons1(:,:), cons2(:,:)
real(kind=dp), allocatable :: prim(:,:), cons0(:,:), cons(:, :)
real(kind=dp), allocatable :: res(:, :), grad(:, :), k1(:,:)
end module variables
