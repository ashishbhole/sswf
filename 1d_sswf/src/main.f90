program main
use variables
use auxillary_conditions
use solver
use postproc
implicit none
integer :: i
character(len = 32) :: arg, input_filename
real(kind=dp) :: src(1:6)

namelist /input_list/ Lx, nc, init_cond, method, T_final, cfl, it_max, it_save

if(iargc() .ne. 1) then
    print*, "Please type : ./sswf input_file_name"
    stop
endif
do i = 1, iargc()
  CALL getarg(i, arg)
  if(i == 1) input_filename = trim(arg)
enddo

write(*,*) " reading input file"
open(1, file=trim(input_filename))
  read(1, input_list)
close(1)
write(*,*) " reading input file complete"

dx = Lx/dble(nc)

allocate(coord(nc))
allocate(cons0(1:6, 0:nc+1), cons(1:6, 0:nc+1))
allocate(prim(1:10, 0:nc+1), res(1:6, 0:nc+1))
allocate(grad(1:6, 0:nc+1))
allocate(k1(1:6, 0:nc+1))
allocate(cons1(1:6, 0:nc+1), cons2(1:6, 0:nc+1))

do i = 1, nc
   coord(i) = i*dx
enddo

it = 0; time = 0.0d0
call initial_condition()
call prim_to_cons()
call cons_to_prim()
call boundary_condition()
call system('rm -rf output')
call system('mkdir output')
call save_solution()

do while ((it .le. it_max) .and. (time .lt. t_final))

   dt = 1.d10
   do i = 1, nc
      speed = dmax1( dabs(prim(2,i)+prim(8,i)) , dabs(prim(2,i)-prim(8,i)) )
      dt = dmin1(dt, cfl * dx / speed)
   enddo
   dt = dmin1(dt, t_final - time)
   !dt = 0.001d0

   if(trim(method) == 'first')then
           
      call cons_to_prim()
      res = 0.0d0
      call boundary_condition()
      call calculate_residue()
      do i = 1, nc
         cons(:,i) = cons(:,i) + dt*res(:,i)/dx 
      end do

   elseif (trim(method)=='second') then

      call cons_to_prim()
      call boundary_condition()
      call G_Grad_X()
      res = 0.0d0
      call calculate_residue()
      do i = 1, nc
         call source(i, src)
         cons(:,i) = cons(:,i) + dt*res(:,i)/dx + dt*src(:)
      end do
      cons0(:,1:nc) = cons(:,1:nc)

      call cons_to_prim()
      call boundary_condition()
      call G_Grad_X()
      res = 0.0d0
      call calculate_residue()
      do i = 1, nc
         call source(i, src)
         cons(:,i) = 0.5d0*(cons0(:,i) + cons(:,i) + dt*res(:,i)/dx) + 0.5d0*dt*src(:)
      end do
           
   endif

   if(trim(init_cond) == 'Roll1D')then
     !call cons_to_prim()
     !call boundary_condition()
     !do i = 1, nc
     !   call source(i, src)
     !   cons(:, i) = cons(:, i) + dt*src
     !enddo
   endif

   it = it + 1
   time = time + dt
   res_norm = 0.0d0

   do i = 1, nc
      res_norm = res_norm + res(1, i)*res(1, i)
   enddo

   print*, it, real(dt), real(time), real(res_norm)

   if(isnan(res_norm))then
      print*, 'code blown up'
      call abort
   endif

   if(mod(it, it_save) == 0) call save_solution()

enddo

call save_solution()

end program main
