module auxillary_conditions
contains
subroutine initial_condition()
use variables
implicit none
integer :: i
real(kind=dp) :: eps = 1.d-12

if(trim(init_cond) == 'DamB1D')then
  do i = 1, nc
     if( (coord(i)-0.5d0) .lt. eps)then
         prim(1, i) = 0.02d0
     else
         prim(1,i) = 0.01d0
     endif
     prim(2, i) = 0.0d0
     prim(3, i) = 0.0d0
     prim(4, i) = 1.0d-4
     prim(5, i) = 0.0d0
     prim(6, i) = 1.0d-4
  enddo

elseif(trim(init_cond) == 'Shear1D')then
  do i = 1, nc
     prim(1, i) = 0.01d0
     prim(2, i) = 0.0d0
     if( (coord(i)-0.5d0) .lt. eps)then
         prim(3, i) = 0.2d0
     else
         prim(3,i) = -0.2d0
     endif
     prim(4, i) = 1.0d-4
     prim(5, i) = 0.0d0
     prim(6, i) = 1.0d-4
  enddo

elseif(trim(init_cond) == 'Roll1D')then
   H_0    = 7.98d-3
   phi    = 22.76d0
   cf     = 0.0036d0
   cr0    = 0.000350d0
   amp    = 0.05d0         ! amplitude of perturbation
   m = 1.0d0
   angle   = 0.05011d0
   U_0 = dsqrt(g * dtan(angle) * H_0 / cf)
  do i = 1, nc
     prim(1, i) = H_0 * (1.0d0 + amp*( dsin(2.0d0*pi*m*coord(i)/Lx) ) )
     prim(2, i) = U_0
     prim(3, i) = 0.0d0
     prim(4, i) = 0.0d0 * phi * prim(1, i) * prim(1, i)
     prim(5, i) = 0.0d0
     prim(6, i) = 0.0d0
  enddo
  
endif

do i = 1, nc
   prim(7,  i) = 0.5d0 * ( g*prim(1, i) + prim(4, i) + Prim(6, i) )
   prim(8,  i) = dsqrt( g *prim(1, i) + 3.0d0 * prim(4, i) )
   prim(9,  i) = dsqrt( prim(4, i) )
   prim(10, i) = 0.5d0 * g * prim(1, i)**2  + prim(4, i) * prim(1, i)
enddo

print*, "!------------------------------------------"
end subroutine initial_condition

subroutine boundary_condition()
use variables
use solver
implicit none

if(trim(init_cond) == 'DamB1D' .or. trim(init_cond) == 'Shear1D')then
   prim(:, 0) = prim(:, 1)
   prim(:, nc+1) = prim(:, nc)
else
   prim(1:6, 0) = prim(1:6, nc-1)
   prim(1:6, 1) = prim(1:6, nc)
   prim(1:6, nc+1) = prim(1:6, 2)
endif
end subroutine boundary_condition

end module auxillary_conditions
