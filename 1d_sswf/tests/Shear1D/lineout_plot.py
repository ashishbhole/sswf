import os
import numpy as np
from matplotlib import pyplot as plt
import sys

fname1 = 'output_first/solution_restart.vtk'
fname2 = 'output_second/solution_restart.vtk'

lw = 2
size = 30

data1 = np.loadtxt(fname1)
data2 = np.loadtxt(fname2)

x1   = data1[:,0]
h1   = data1[:,1]
u1   = data1[:,2]
v1   = data1[:,3]
pxx1 = data1[:,4]
pxy1 = data1[:,5]
pyy1 = data1[:,6]

x2   = data2[:,0]
h2   = data2[:,1]
u2   = data2[:,2]
v2   = data2[:,3]
pxx2 = data2[:,4]
pxy2 = data2[:,5]
pyy2 = data2[:,6]

plt.subplot(3, 1, 1)
plt.plot(x1, v1, '-',linewidth=lw)
plt.plot(x2, v2, '--',linewidth=lw)
plt.ylabel(r'$v$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.subplot(3, 1, 2)
plt.plot(x1, pxy1, '-',linewidth=lw)
plt.plot(x2, pxy2, '--',linewidth=lw)
plt.ylabel(r'$P_{xy}$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.subplot(3, 1, 3)
plt.plot(x1, pyy1, '-',linewidth=lw)
plt.plot(x2, pyy2, '--',linewidth=lw)
plt.xlabel(r'$x$', fontsize=size)
plt.ylabel(r'$P_{yy}$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.savefig('Shear1D.pdf')
plt.show()
