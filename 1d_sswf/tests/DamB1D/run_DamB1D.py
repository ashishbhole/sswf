import os
import numpy as np
from matplotlib import pyplot as plt
import argparse
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove

# get current directory 
current_dir = os.getcwd()

#remove old solution directories
command = 'rm -r output_*'
os.system(command)

# path to executable
exe = '../../src/sswf'
# check that executable is present
if os.path.isfile(exe)==False:
    print "Could not find ", exe
    exit()

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

print 'Running with first'
print '   Running sswf'
os.system(exe+' DamB1D.in > out')
command = 'output_first'
os.system('mv output '+command)

print 'Running with second'
replace('DamB1D.in', "method = 'first'", "method = 'second'")
print '   Running sswf'
os.system(exe+' DamB1D.in > out')
command = 'output_second'
os.system('mv output '+command)

#restore
replace('DamB1D.in', "method = 'second'", "method = 'first'")

command = 'python lineout_plot.py '
print command
os.system(command)   
