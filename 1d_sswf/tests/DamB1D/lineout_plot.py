import os
import numpy as np
from matplotlib import pyplot as plt
import sys

fname1 = 'output_first/solution_restart.vtk'
fname2 = 'output_second/solution_restart.vtk'

lw = 2
size = 30

data1 = np.loadtxt(fname1)
data2 = np.loadtxt(fname2)

x1   = data1[:,0]
h1   = data1[:,1]
u1   = data1[:,2]
v1   = data1[:,3]
pxx1 = data1[:,4]
pxy1 = data1[:,5]
pyy1 = data1[:,6]

x2   = data2[:,0]
h2   = data2[:,1]
u2   = data2[:,2]
v2   = data2[:,3]
pxx2 = data2[:,4]
pxy2 = data2[:,5]
pyy2 = data2[:,6]

plt.subplot(3, 1, 1)
plt.plot(x1, h1, '-',linewidth=lw)
plt.plot(x2, h2, '--',linewidth=lw)
plt.ylabel(r'$h$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.subplot(3, 1, 2)
plt.plot(x1, u1, '-',linewidth=lw)
plt.plot(x2, u2, '--',linewidth=lw)
plt.ylabel(r'$u$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.subplot(3, 1, 3)
plt.plot(x1, pxx1, '-',linewidth=lw)
plt.plot(x2, pxx2, '--',linewidth=lw)
plt.xlabel(r'$x$', fontsize=size)
plt.ylabel(r'$P_{xx}$', fontsize=size)
plt.legend(('first', 'second'))
plt.grid(True)

plt.savefig('DamB1D.pdf')
plt.show()
