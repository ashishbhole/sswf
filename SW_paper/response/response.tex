\documentclass[pdf]{article}
\include{DefMy}

\title{Response to the reviews of: Fluctuation splitting Riemann solver for a non-conservative modeling of shear shallow water flow} 
\author{}
\date{}
\begin{document}
\maketitle

\begin{center}
\textbf{Response to Reviewer 1:} 
\end{center}

The paper "Fluctuation splitting Riemann solver for a non-conservative modeling of shear shallow water flow" by A. Bhole, B. Nkonga, S. Gavrilyuk and K. Ivanova proposes a new fluctuation splitting finite volume scheme for a non-conservative model of shear shallow water flow.
\\
\par The proposed numerical method is based on splitting the dissipative model into non-dissipative and dissipative parts. Then, the non-dissipative system is splitted into shear and acoustic sub-systems, giving rise to two associated Riemann problems. The Riemann problem for the shear sub-system can be solved exactly, while an approximate HLL-type Riemann solver can be applied to the acoustic part. Since the system is non-conservative, the authors follow the DLM approach and define a family of paths to design approximate Riemann solver. The resulting finite volume scheme is validated on several numerical tests.
\\
\par  There are several minor remarks that need to be addressed in order for the paper to be published in JCP. \\

1. p. 3: "the eigenvalues of the system (3.1)" probably means the eigenvalues of the flux Jacobian A multiplied by normal vector n.\\
{\color{blue} \textbf {Response:} Yes, we have modified the writing accordingly. This is highlighted in blue color, below Eq. (3.1) in the revised manuscript. Thank you for pointing out this deficiency.} \vspace*{3mm} \\

2. p. 7: shouldn't there be $3P_{nn}$ in the formula for $\lambda_{+}^a$? \\
{\color{blue} \textbf {Response:} Yes, it was a typing mistake from us and we have corrected it. Thank you for pointing it out.}
\vspace*{3mm} \\

3. p. 7: the formulations like "eigenvalue X is linearly degenerate" should be rephrased e.g. as "the characteristic field associated to the eigenvalue X is linearly degenerate".\\
{\color{blue} \textbf {Response:} Yes, we have modified the writing accordingly. This is highlighted in blue color on page 8 in the revised manuscript. Thank you for pointing out this deficiency.} \vspace*{3mm} \\

4. p. 9: in the discussion of the family of paths, you say "These paths, in the phase space, will connect the two states on either side of the shock". Does it mean that the states are connected by a straight line segment? Is it possible to provide explicit expressions for these paths?\\
{\color{blue} \textbf {Response:} The paths are defined by a numerical jump condition that enforces the entropy inequality. We have added such text, highlighted in blue color, on page 10 and the assumption behind choosing these paths is highlighted in red color.} \vspace*{3mm} \\

5. p. 11-12: are subcells introduced in order to perform the computations on arbitrary polygonal meshes? The notation $C_{ij}$ was not introduced.\\
{\color{blue} \textbf {Response:} We have introduced the notation $C_{ij}$ (pictorially in Fig. 1). Yes, sub-cells are introduced on arbitrary polygonal cells. We have added this fact explicitly in the revised manuscript which is highlighted in blue color on page 12.}  \vspace*{3mm} \\

6. p. 11: are the integrals in (4.1) approximated by some sort of quadrature? How many evaluations of Riemann solver are needed?\\
{\color{blue} \textbf {Response:} Evaluation of these integrals is denoted by Eq. (3.26) and (3.27). At each edge of the polygonal mesh, only one evaluation of Riemann solver is needed that gives fluctuations associated with both shear and acoustic sub-systems. We have added one sentence to highlight this fact on page 12, just before Section 4.2.} \vspace*{3mm} \\

7. p. 14, why do you use CFL=0.5, isn't it too low for 1D first-order finite volume scheme?\\
{\color{blue} \textbf {Response:} We have chosen CFL arbitrarily. We can produce the results with larger CFL as well. The results shown in the Fig. (\ref{damb1d}) below are produced with CFL = 0.9.} \vspace*{3mm} \\

8. p. 17: in the discussion about efficiency, you say "In [7], equation of energy conservation has been solved along with the SSWF model (2.1), whereas in this paper only system (2.1) has been solved. This certainly gives an advantage on the cost of computations." I don't think that this is a fair comparison of computational efficiency of different methods. However I believe that a comparison of the computational efficiency of the proposed flux splitting Riemann solver and different Riemann solvers applied to the unsplitted system would be desirable (e.g. in terms of accuracy vs CPU time). Otherwise it's not quite clear what you gain by performing the splitting. \\
{\color{blue} \textbf {Response:} The accuracy vs CPU time studies is not feasible as both solvers/codes do not use the same data-structures. The solver in [7] is written for structured grid problems while the current solver is in unstructured grid framework. Unstructured grid solvers are known to be slower than their structured counter-parts. We argued the claim about the cost of computation based on the fact that, in [7], at each edge two Riemann solvers are needed to be evaluated with energy as an additional variable, while here we need only one Riemann solver. Yet, to clear the confusion, we have removed the sentence from the Conclusion Section: `This certainly gives an advantage on the cost of computations.'} \vspace*{3mm} \\
\newpage

\begin{center}
\textbf{Response to Reviewer 2:} 
\end{center}

In this manuscript, the authors propose a fluctuation splitting finite volume scheme for a non-conservative modeling of shear shallow water flow. The scheme is based on the physical splitting which is easily applied to unstructured grids. I have some comments as follows:\\

1. Two authors of this manuscript published a work on the same problem in [Gavrilyuk et. al. J. Comput. Phys., 366, 252-280, 2018]. Compared with their previous work, the contribution of this work is only to design Riemann solvers that can be used in the numerical scheme built on unstructured grids.\\
{\color{blue} \textbf {Response:} To emphasis on the difference between two works, we have added explanation in Section 3.2 of the revised manuscript which is highlighted in red color.}
\vspace*{3mm} \\

However, the scheme is still first order. \\
{\color{blue} \textbf {Response:} Extension of the scheme to second order is not straight-forward. One needs to evaluate the fluctuations in the cells as well, as governing equations are non-conservative. During reconstruction step, multiple states arise at the point $\mathcal{C}_i$ in the Fig. (1) of the manuscript and hence a multi-dimensional Riemann solver \cite{Ketcheson2013, Balsara2016, Vides2015} (referred below) may be needed in such case. This task is reserved as a future work. 

However, in 1D, the second order scheme can be constructed based on the strategy proposed in Section 11.1 of \cite{Sergey2018} (referred below) where the fluctuation in the cells are taken into account as well. We follow MUSCL's approach using Van Leer limited gradients and Heun's time-integration method to get the second order scheme. The comparison of results for 1D shear problem, obtained with the first and second order scheme, is shown in Fig. (\ref{shear1d_comp}) below with 500 grid points and $\Delta t = 0.001$. The comparison of results for 1D dam break problem, obtained with the first and second order scheme, is shown in Fig. (\ref{damb1d_comp}) below with 500 grid points and $\Delta t = 0.0001$.}
\vspace*{3mm} \\

In order to capture the shock accurately, a very refined mesh 
must be used (See Figs. 6 and 7).\\
{\color{blue} \textbf {Response:} Choice of mesh with 10000 points is arbitrary and is made just to compare the results with those in [7]. We can capture the shock with 2000 points (and first order scheme) also, as shown in the Fig. (\ref{damb1d}) below.}
\vspace*{3mm} \\

2. In section 3, there are too many notations with subscript or superscript, the authors do not give explicit explanations. They are ambiguous.\\
{\color{blue} \textbf {Response:} We have re-written some parts of the manuscript and re-defined the notations to overcome this deficiency. Thank you for pointing out this deficiency.}
\vspace*{3mm} \\

3. In section 4.2, the definition of $C_{ij}$ is not clear, which should be shown in Fig. 1.\\
 {\color{blue} \textbf {Response:} We have shown the notation of $C_{ij}$ in Fig. 1 of the revised manuscript. We have added the explanation for sub-cells in the revised manuscript which is highlighted in blue color on page 12.}
\vspace*{3mm} \\

4. On page 11, line 6, $U_{i-1/2}(t^{n+1},n)$ should be $U_{i-1/2}(t^{n+1},x), U_{i+1/2}(t^{n+1},n)$ should be $U_{i+1/2}(t^{n+1},x)$\\
{\color{blue} \textbf {Response:} Yes, it was a mistake from us. We have corrected it on page 12 of the revised manuscript. Thank you for pointing it.} 
\vspace*{3mm} \\

I think that this manuscript is not enough to publish in high level journals, such as Journal of Computational Physics.

\begin{thebibliography}{10}

\bibitem{Ketcheson2013}
D.~I.~Ketcheson, M.~Parsani, and R.~J.~LeVeque.
 \newblock High-Order Wave Propagation Algorithms for Hyperbolic Systems. \newblock {\em SIAM Journal on Scientific Computing}, 35(1), A351 -- 377, 2013.		

\bibitem{Balsara2016}
D.~ S.~Balsara, J.~Vides,  K.~Gurskic,  B.~Nkonga, M.~Dumbser, S.~Garain, E.~Audite. \newblock A two-dimensional Riemann solver with self-similar sub-structure - Alternative formulation based on least squares projection. \newblock {\em Journal of Computational Physics}, 304: 138 - 161, 2016.
  
\bibitem{Vides2015}
 J.~Vides, B.~Nkonga and E.~Audite. \newblock A simple two-dimensional extension of the HLL Riemann solver for hyperbolic systems of conservation laws. \newblock {\em Journal of Computational Physics}, 280: 643 -- 675, 2015.


\bibitem{Sergey2018}
S.~Gavrilyuk, B.~Nkonga, K.~Shyue, L.~Truskinovsky. \newblock Generalized Riemann problem for dispersive equations, \newblock {\em submitted}, 2018 <hal-01958328> \url{https://hal.archives-ouvertes.fr/hal-01958328/document}

\end{thebibliography}


\begin{figure}[H]
\center
  \includegraphics[width=15cm]{DamB1D}
  \caption{Numerical solution of 1D dam-break test showing $h$, $u$ and $\mathbb{P}_{xx}$ (with first order scheme) on the different grids with 100, 500 and 2000 grid cells. The CFL is specified as 0.9.}
\label{damb1d}
\end{figure}

\begin{figure}[H]
\center
  \includegraphics[width=15cm]{Shear1D_comp}
  \caption{Comparison of the numerical solutions of 1D shear test case showing $v$, $\mathbb{P}_{xy}$ and $\mathbb{P}_{yy}$ with first and second order scheme with 500 grid points. The time-step is specified as 0.001.}
\label{shear1d_comp}
\end{figure}


\begin{figure}[H]
\center
  \includegraphics[width=15cm]{DamB1D_comp}
  \caption{Comparison of the numerical solutions of 1D dam break test case showing $h$, $u$ and $\mathbb{P}_{xx}$ with first and second order scheme with 500 grid points. The time-step is specified as 0.0001}
\label{damb1d_comp}
\end{figure}

\end{document}
