settings.outformat="pdf";
settings.render=3;
size(10cm,0);
import math;

pair[] x = new pair[6];
pair[] c = new pair[4];
pair[] m = new pair[2];

x[0] = (0,0); x[1] = (1,0); x[2] = (0.7,1);
x[3] = (0.4,-0.8); x[4] = (1.5, 0.8); x[5] = (-0.4, 0.5); x[6] = (0.2, 1.3);

c[0] = (x[0]+x[1]+x[2])/3.0;
c[1] = (x[0]+x[1]+x[3])/3.0;
c[2] = (x[1]+x[2]+x[4])/3.0;
c[3] = (x[2]+x[0]+x[5]+x[6])/4.0;

pen p=linewidth(1mm);
dot(x[0], p); dot(x[1], p); dot(x[2], p);
dot(x[3], p); dot(x[4], p); dot(x[5], p);  dot(x[6], p);

dot(c[0], p); dot(c[1], p); dot(c[2], p); dot(c[3], p); 

//label("$C_i$",c[0], S);
//label("$C_j$",c[2], N);

pen p=linewidth(0.5mm);
draw(x[0]--x[1], red+p);
draw(x[1]--x[2], red+p);
draw(x[2]--x[0], red+p);

draw(x[0]--x[3], p);
draw(x[1]--x[3], p);

draw(x[1]--x[4], p);
draw(x[2]--x[4], p);

draw(x[0]--x[5], p);
draw(x[5]--x[6], p);
draw(x[6]--x[2], p);

m[0] = (x[1]+x[2])/2.0;
m[1] = (m[0]+c[2])/2.0;

draw(m[0]--m[1], EndArrow);
//label("$\mathbf{n}_{ij}$", m[0]--m[1], SE);

pen p=linewidth(0.25mm);
draw(c[0]--x[0], green+p+dashed);
draw(c[0]--x[1], green+p+dashed);
draw(c[0]--x[2], green+p+dashed);

draw(c[1]--x[0], p+dashed);
draw(c[1]--x[1], p+dashed);
draw(c[1]--x[3], p+dashed);

draw(c[2]--x[1], p+dashed);
draw(c[2]--x[2], p+dashed);
draw(c[2]--x[4], p+dashed);

draw(c[3]--x[2], p+dashed);
draw(c[3]--x[0], p+dashed);
draw(c[3]--x[5], p+dashed);
draw(c[3]--x[6], p+dashed);
