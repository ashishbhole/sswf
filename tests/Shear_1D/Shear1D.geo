lx = 1.0;
Np = 5000;

// choice of meshing algorithm: default is 2
// 1 = MeshAdapt, 2 = Automatic, 5 = Delaunay
// 6 = Frontal, 7 = BAMG, 8 = DelQuad
Mesh.Algorithm = 6;

Point(1) = { 0,  0, 0};
Point(2) = {lx,  0, 0};

Line(1) = {1, 2};

Transfinite Line{1} = Np;

lc = 0.01;

Extrude {0, lc, 0} {Line{1}; Layers{6}; Recombine;}

Physical Surface(101) = {5};
Physical Line(1) = {1};  // bottom
Physical Line(2) = {2};  // top
Physical Line(3) = {3};  // left
Physical Line(4) = {4};  // right
