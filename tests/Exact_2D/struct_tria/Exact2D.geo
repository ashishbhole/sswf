//
//             3
//      ----------------
//      |              |
//      |              |
//   4  |              |  2
//      |              |
//      ---------------
//            1

lx = 10.0; ly = 10.0;

char_len = 1.0;

// choice of meshing algorithm: default is 2
// 1 = MeshAdapt, 2 = Automatic, 5 = Delaunay
// 6 = Frontal, 7 = BAMG, 8 = DelQuad
Mesh.Algorithm = 6;
//Mesh.RecombinationAlgorithm = 1;
//Mesh.SubdivisionAlgorithm = 1;

Point(1) = { 0,  0, 0, char_len};
Point(2) = {lx,  0, 0, char_len};
Point(3) = {lx, ly, 0, char_len};
Point(4) = {0,  ly, 0, char_len};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(1) = {1,2,3,4};
Plane Surface(1) = {1};

Transfinite Line{1,2,3,4} = lc;
Transfinite Surface {1} = {1,2,3,4};
//Recombine Surface {1};

//Periodic Line {1} = {3};  // periodic in x
//Periodic Line {2} = {-4};  // periodic in y

Physical Line(1) = {1};  // x dir: left 
Physical Line(2) = {2};  // x dir: right
Physical Line(3) = {3};  // y dir: bottom
Physical Line(4) = {4};  // y dir: top
Physical Surface(101) = {1};
