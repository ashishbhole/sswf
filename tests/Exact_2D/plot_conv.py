import os
import numpy as np
from matplotlib import pyplot as plt

fact = 1.0 # 0.001
# read error from file
data = np.loadtxt('conv.txt')
plt.loglog(data[:,0],fact*data[:,0], linewidth=2)
plt.loglog(data[:,0],data[:,1],'o-', linewidth=2)
plt.loglog(data[:,0],data[:,2],'*--',linewidth=2)
plt.loglog(data[:,0],data[:,3],'s-.',linewidth=2)
plt.loglog(data[:,0],data[:,4],':', linewidth=2)
plt.loglog(data[:,0],data[:,5],'-.',linewidth=2)
plt.loglog(data[:,0],data[:,6],'--',linewidth=2)
plt.xlabel('h')
plt.ylabel('Error')
plt.legend(('1st', 'h', 'u', 'v', 'P11', 'P12', 'P22'))
plt.savefig('error_l1.pdf')
plt.show()

plt.loglog(data[:,0],fact*data[:,0], linewidth=2)
plt.loglog(data[:,0],data[:,7],'o-', linewidth=2)
plt.loglog(data[:,0],data[:,8],'*--',linewidth=2)
plt.loglog(data[:,0],data[:,9],'s-.',linewidth=2)
plt.loglog(data[:,0],data[:,10],':', linewidth=2)
plt.loglog(data[:,0],data[:,11],'-.',linewidth=2)
plt.loglog(data[:,0],data[:,12],'--',linewidth=2)
plt.xlabel('h')
plt.ylabel('Error')
plt.legend(('1st', 'h', 'u', 'v', 'P11', 'P12', 'P22'))
plt.savefig('error_l2.pdf')
plt.show()

plt.loglog(data[:,0],fact*data[:,0], linewidth=2)
plt.loglog(data[:,0],data[:,13],'o-', linewidth=2)
plt.loglog(data[:,0],data[:,14],'*--',linewidth=2)
plt.loglog(data[:,0],data[:,15],'s-.',linewidth=2)
plt.loglog(data[:,0],data[:,16],':', linewidth=2)
plt.loglog(data[:,0],data[:,17],'-.',linewidth=2)
plt.loglog(data[:,0],data[:,18],'--',linewidth=2)
plt.xlabel('h')
plt.ylabel('Error')
plt.legend(('1st', 'h', 'u', 'v', 'P11', 'P12', 'P22'))
plt.savefig('error_linf.pdf')
plt.show()
