import os
import numpy as np
from matplotlib import pyplot as plt
import argparse

# remove old files
os.system("rm -f conv.txt")

# get current directory 
current_dir = os.getcwd()

# name of geo file
geofilename = 'Exact2D.geo'

# remove old msh file
command = 'rm Exact2D.msh'
os.system(command)

# path to executable
exe = os.environ.get('SSWF_DIR')+'/sswf'
# check that executable is present
if os.path.isfile(exe)==False:
    print "Could not find ", exe
    exit()

# change no of points in the x or y-direction
lc = [40, 80, 160, 320]

for n in range(len(lc)):
   print 'Running with lc = '+str(lc[n])
   # generate msh file using gmsh -3 command
   print '   Running gmsh'
   command = 'gmsh -2 '+geofilename+' -setnumber lc '+str(lc[n])+' -o Exact2D.msh'
   os.system(command+' > gmsh.log')
   # run preprocessor on current level
   print '   Running sswf'
   os.system(exe+' Exact2D.in > out')
