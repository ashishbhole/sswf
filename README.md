Code Name: sswf (shear shallow Water flows)

A 2D finite volume code on the unstructured meshes with mixed
unstructured mesh. Aim is to validate the newly proposed FVM for
shear shallow water equations for unstructured meshes.

In case of any bugs or problems while using code, please write to
ashish.bhole@inria.fr

Important things before using code:

1) Set up the input and geo file for a test case

2) Make sure that name of msh file is set correctly in input file

3) Make sure that required parameters like boundary tags and domain length 
   are consistently set in input and msh file.

How to use code:

1) set an enviornmental path 'SSWF_DIR' pointing to /path-to/sswf/src in .bashrc file

2) go to the the directory $SSWF_DIR/src and run the makefile to compile the code
   'make'
   This will generate an executable 'sswf'

3) go to the directory containing set-up of test in sswf/tests

4) set-up a .geo file to generate a mesh, e.g.
   'gmsh -2 test.geo'

   Gmsh program is needed for this.

5) set up input file e.g. 'test.in'

6) run the code using
   '$SSWF_DIR/sswf test.in'

7) results will be generated in the './output' directory in vtk files.
   VisIt can be used visualize the results.

For convergence test:

1) inside tests/Exact2D directory there are 2 python files.

2) use following command to run this test cases for series of grids.
  'python run_Exact2D.py'

   User has to specify no of grid points for grid series in the run_Exact2D.py.

3) use following command to visualize the error converegence.
  'python plot_conv.py'
